﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialMenu : Menu {

	GameObject image;
	GameObject movie;
	MovieTexture mt;
	RawImage rim;

	Sprite xboxPad;
	Sprite page1;
	Sprite page2;
	Sprite page3;

	// Use this for initialization
	void Start () {
		image = GameObject.Find("Menus/MinimalPanel - Private/Image");
		movie = GameObject.Find("Menus/MinimalPanel - Private/Movie");
		rim = movie.GetComponent<RawImage> ();
		mt =  (MovieTexture)movie.GetComponent<RawImage> ().mainTexture;
		rim.enabled = true;
		mt.loop = true;
		mt.Play ();

		xboxPad =  Resources.Load("Sprites/XBOXPad", typeof(Sprite)) as Sprite;
		page1 =  Resources.Load("Sprites/PageOne", typeof(Sprite)) as Sprite;
		page2 =  Resources.Load("Sprites/Attacks", typeof(Sprite)) as Sprite;
		page3 =  Resources.Load("Sprites/Shields", typeof(Sprite)) as Sprite;

	}
	
	// Update is called once per frame
	void Update () {
		if (PadButtons.back_button) {
			PadButtons.back_button = false;
			ExitToMenu ();	
		}
		if (PadButtons.B_button) {
			MovieButton ();
		}
		if (PadButtons.start_button) {
			PadButton ();
		}
		if (PadButtons.X_button) {
			Page01 ();
		}
		if (PadButtons.Y_button) {
			Page02 ();
		}
		if (PadButtons.A_button) {
			Page03 ();
		}
	}

	void ResetImageMovie()
	{
		image.GetComponent<Image> ().enabled = false;
		rim.enabled = false;
		mt.Stop ();
	}

	void SetImage(Sprite sprite)
	{
		image.GetComponent<Image>().enabled = true;
		image.GetComponent<Image>().sprite = sprite;
	}

	public void MovieButton()
	{
		ResetImageMovie ();
		rim.enabled = true;
		mt.Play ();
	}

	public void PadButton()
	{
		ResetImageMovie ();
		//get drawing from resources
		SetImage(xboxPad);
	}

	public void Page01()
	{
		ResetImageMovie ();
		//get drawing from resources
		SetImage(page1);
	}

	public void Page02()
	{
		ResetImageMovie ();
		//get drawing from resources
		SetImage(page2);
	}

	public void Page03()
	{
		ResetImageMovie ();
		//get drawing from resources
		SetImage(page3);
	}


}
