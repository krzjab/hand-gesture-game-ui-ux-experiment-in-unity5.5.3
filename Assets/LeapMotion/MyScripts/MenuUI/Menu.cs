﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

	bool exitTrigger = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	


	}

	public void Button01()
	{
		Debug.Log ("Button01 Clicked");
	}

	public void Button02()
	{
		Debug.Log ("Button02 Clicked");
	}

	public void Button03()
	{
		Debug.Log ("Button03 Clicked");
	}

	public void TutorialButton()
	{
		Debug.Log ("TutorialScene should load");
		SceneManager.LoadScene("LeapMotion/MyScenes/VR_KJ_TG02", LoadSceneMode.Single);
	}

	public void PlayGroundButton()
	{
		Debug.Log ("PlayScene should load");
		SceneManager.LoadScene("LeapMotion/MyScenes/VR_KJ_PG01", LoadSceneMode.Single);
	}

	public void FightGroundButton()
	{
		Debug.Log ("FightScene should load");
		SceneManager.LoadScene("LeapMotion/MyScenes/VR_KJ_FG01", LoadSceneMode.Single);
	}

	public void ExitGameButton()
	{
		Application.Quit();
	}

	public void ExitToMenu()
	{
		Debug.Log ("MenuScene should load");
		SceneManager.LoadScene("LeapMotion/MyScenes/VR_KJMenu01", LoadSceneMode.Single);
	}
		

}
