﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : Menu {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (PadButtons.back_button) {
			PadButtons.back_button = false;
			ExitGameButton ();	
		}
		if (PadButtons.start_button) {
			TutorialButton ();
		}
		if (PadButtons.Y_button) {
			PlayGroundButton ();
		}
		if (PadButtons.X_button) {
			FightGroundButton ();
		}


	}
}
