﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using Leap;
using Leap.Unity;
using UnityEngine.UI;

public class VelocityPPDetector : Detector {

	LeapProvider provider;

	Vector3 velocityV;
	Vector3 palmPosition;

	Vector3 velocityVMax; //maximal velocity in this runtime
	Vector3 velocityVOnValue; // empiric testing for this value

	[Tooltip("Add a Object to Inspector for this")]
	public GameObject positionObject;

	public bool velocityLogic;
	public bool palmPositionLogic;

	GameObject canvas_VPPD;

	//Detector stuff
	private float Period;//seconds

	public IEnumerator watcherCourtine;


	// Use this for initialization
	void Start () {
		//watcherCourtine = 
		velocityVMax = Vector3.zero;
		velocityVOnValue = new Vector3 (-1.5f, 0.0f, 0.0f);
		Period = 0.05f;
		provider = FindObjectOfType<LeapProvider> () as LeapProvider;
		canvas_VPPD = GameObject.Find ("Canvas_VPPD");

		StartCoroutine ("SlowUpdateCourtine");
	}
	
	// Update is called once per frame
	void Update () {
		if (velocityV.x < velocityVMax.x){
			velocityVMax = velocityV;
		}
	}

	public IEnumerator SlowUpdateCourtine()
	{
		while (true) {
			Frame frame = provider.CurrentFrame;
			foreach (Hand hand in frame.Hands) {
				if (!hand.IsLeft) {
			
					palmPosition = hand.PalmPosition.ToVector3 ();
				//	Debug.Log ("PalmPosition: X: " + palmPosition.x + " Y: " + palmPosition.y + " Z: " + palmPosition.z);
					velocityV = hand.PalmVelocity.ToVector3 ();
				//	Debug.Log ("PalmVelocity: X: " + velocityV.x + " Y: " + velocityV.y + " Z: " + velocityV.z);

					canvas_VPPD.GetComponentInChildren<Text> ().text = "PalmPosition: X: " + palmPosition.x + " Y: " + palmPosition.y + " Z: " + palmPosition.z + "\n" + "PalmVelocity: X: " + velocityV.x + " Y: " + velocityV.y + " Z: " + velocityV.z + "\n" + "MAX_PalmVelocity: X: " + velocityVMax.x + " Y: " + velocityVMax.y + " Z: " + velocityVMax.z;


					//position Logic
					if (palmPosition.x < positionObject.transform.position.x) {
						palmPositionLogic = true;
					} else {
						palmPositionLogic = false;
					}
					// velocity Logic
					if (velocityV.x <= velocityVOnValue.x) {
						velocityLogic = true;
					} else {
						velocityLogic = false;
					}


					//WholeLogicGate
					if (palmPositionLogic && velocityLogic) {
						Activate ();
					} else {
						Deactivate ();
					}

				}
			}
			yield return new WaitForSeconds (Period);
		}
	}


}
