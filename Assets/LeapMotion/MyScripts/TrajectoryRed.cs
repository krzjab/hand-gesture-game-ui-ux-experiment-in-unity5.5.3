﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Leap;
using Leap.Unity;
using UnityEngine.Events;
using UnityEngine.UI;
using ElementsGame;


public class TrajectoryRed : TrajectoryBase {



	// Use this for initialization
	override protected void Start () {
		base.Start ();
		signMsg = "RED SIGN COMPLETED";
		signTextColor = Color.red;
		signColor = GameMaster.Elements.FIRE;

	}
	
	// Update is called once per frame
	void Update () {

	}



	void TrajectoryCompleted()
	{
		
	}

	override protected void TrajectoryDone()
	{
		base.TrajectoryDone();
		//GameMaster.OnRed += RedMessage;
		Debug.Log ("RedMessage added to OnRed");
		//SignMessage ();
		RedMessage ();
		// add message for gamemaster
		//RedMessage();
	}

	void RedMessage()
	{
		
		Debug.Log("RED SIGN COMPLETED");
		//tutorial unlockings
		if (GameMaster.Tut_actions_check (3)) {
			GameMaster.Tut_action_set (4,true);
		}


	}


	override protected void SetCheckpoints()
	{
		cp = new GameObject[3];
		cp [0] = GameObject.Find ("SR1");
		cp [1] = GameObject.Find ("SR2");
		cp [2] = GameObject.Find ("SR3");

	}

	override protected void SetMaterials()
	{
		whiteMat =  Resources.Load("Materials/White", typeof(Material)) as Material;
		signMat = Resources.Load("Materials/Red", typeof(Material)) as Material;
	}


}
