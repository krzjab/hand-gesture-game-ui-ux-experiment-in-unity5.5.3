﻿using UnityEngine;
using System.Collections;
using Leap;
using Leap.Unity;
using UnityEngine.UI;

public enum GameMode { TestModeCanvas, RedMode, BlueMode, YellowMode, BrownMode, AIMode }
public enum WinLose { Win, Lose, Start }

namespace ElementsGame
{

	public class GameMaster : MechanicsManager {

		//GameMode - 1/2- Disabled by developer START
		/*
		public GameMode currentGameMode = GameMode.AIMode;
		*/
		//Disabled by developer END

		public static bool gameMode_test_isActive = false; // enables canvas UI msg for canvas testing, methods in GestureWatcher if statements
		public static bool gameMode_gestureQ_isActive = true; // set true to count on canvas gestures quantity
		public static bool gameMode_PlayGroundMode = false;
		public static bool gameMode_TutorialGroundMode = false;


			//public static int[] Signs = new int[5] {(int)Elements.NONE, (int)Elements.NONE, (int)Elements.NONE, (int)Elements.NONE, (int)Elements.NONE};

		public static int scoreWIN = 0;
		public static int scoreLose = 0;

		LeapProvider provider;
		private float Period = 0.01f;//seconds

		private GameObject tRed;
		private GameObject tYellow;
		private GameObject tViolet;
		private GameObject tBrown;
		private GameObject tBlue;

		public static bool powerChargingRight = false;
		public static bool powerChargingLeft = false;

		private bool doubleHandMissleCastRightH = false;
		private bool doubleHandMissleCastLeftH = false;
		private bool doubleHandCast = true;

		private bool resetSignsGestureRH = false;
		private bool resetSignsGestureLH = false;
		private bool resetSignsGestureReady = true;

		private bool oneHandCast = true;

		private bool upgrade = true;

		public static bool shieldPoseHandRight = false;
		public static bool shieldPoseHandLeft = false;

		private GameObject canvasLeft;
		private GameObject canvasRight;
		private GameObject canvasGQ;

		private bool cancelSignsActive = false;

		public static float speedSmallMissile = 2.0f;
		public static float speedLargeMissile = 1.5f;
		public static float speedEnemyMissile = 1.0f;


		public static void Tut_actions_reset()
		{
			for (int i = 0; i < 23; i++) {
				tut_actions [i] = false;
			}
			tut_actions [0] = true;
		}

		public static bool Tut_actions_check(int action_nr)
		{
			if (tut_actions [action_nr] == true && tut_actions[action_nr +1] == false) {
				return true;
			} else {
				return false;
			}
		}

		public static void Tut_action_set(int action_nr, bool actionIsActive)
		{
			tut_actions [action_nr] = actionIsActive;
		}

	

		void GetCanvas()
		{
			canvasLeft = GameObject.Find ("Canvas_Left");
			canvasRight = GameObject.Find ("Canvas_Right");
		}

		void GetTrajectorys()
		{
			tRed = GameObject.Find ("TrajectoryRed");
			tYellow = GameObject.Find ("TrajectoryYellow");
			tViolet = GameObject.Find ("TrajectoryViolet");
			tBrown = GameObject.Find ("TrajectoryBrown");
			tBlue = GameObject.Find ("TrajectoryBlue");
		}

	

		void Awake()
		{

		}

		// Use this for initialization
		override protected void Start () {
			//gamemode


			base.Start ();
			GetTrajectorys ();
			GetCanvas ();
			SignsSet ();
			GestureQuantityCanvasSet ();
			WinLoseCanvasSet (WinLose.Start);

			provider = FindObjectOfType<LeapProvider> () as LeapProvider;

			//GameMode - 2/2 - Disabled by developer START
			/*
			currentGameMode = GameMode.AIMode;
			*/
			//Disabled by developer END
			gameMode_test_isActive = false;



			StartCoroutine ("GestureWatcherCourtine"); //gesture watcher


			if (gameMode_test_isActive == true) {
				//test Canvas instantiate:
				CanvasInstantiate ("moja testowa wiadomosc", Color.red);
				CanvasInstantiate ("moja testowa wiadomosc v02", Color.red);

				//test Missile
				MissileSpawn(Target.Enemy, 2.0f, MissileSize.H1, MissileType.RED);
				//test missile 2
				MissileSpawn(Target.Player, 2.0f , MissileSize.H1, MissileType.YELLOW);
				MissileSpawn (Target.Enemy, 1.5f, MissileSize.H2, MissileType.BLUE);
			}
			//set tutorial actions
			Tut_actions_reset ();

		}
		
		// Update is called once per frame
		void Update () {

			StartCoroutine( SmallMissilesPad() );
			StartCoroutine (LargeMissilesPad ());
			StartCoroutine (UpgradesPad ());
			PadActionsUpdate();
			ChargingPad(); // Charging Players Energy Point
			ShieldActivePad(); // Charging Players Shield Points
			GestureQuantityCanvasUpdate();
		}



		public IEnumerator GestureWatcherCourtine()
		{
			while (true) {
				Frame frame = provider.CurrentFrame;

				SignsDraw ();

				/* Cancel Signs Functionality - Disabled by Developer 1/2
				// Cancel Signs - cancel signs gesture
				if (frame.Hands.Count == 0 && cancelSignsActive) {
					Debug.Log ("Cancel Signs - NO Hands in sensors Cone");
					CanvasInstantiate ("Signs Canceled - No Hands in sensors Cone", Color.black);
					SignsReset ();
					cancelSignsActive = false;
				}
				*/
				/* Cancel Text on Canvas - Disabled by developer
				// Left Canvas Right Canvas Reset
				if (frame.Hands.Count == 0) {
					canvasLeft.GetComponentInChildren<Text> ().text = "";
					canvasRight.GetComponentInChildren<Text> ().text = "";
				}
				*/

				// Gestures Checks
				foreach (Hand hand in frame.Hands) {

					/* Cancel Signs Functionality - Disabled by Developer 2/2
					cancelSignsActive = true;
					*/

					if (!hand.IsLeft) {

						Red (hand);
						Yellow (hand);
						Violet (hand);
						Brown (hand);
						Blue (hand);

						//TestMissle (hand);
						if (gameMode_test_isActive == false){
							StartCoroutine (SmallMissiles (hand)); // small missiles Offensive Actions
							StartCoroutine(Upgrades(hand)); // upgrades, Deffensive actions
						}
						if (gameMode_test_isActive == true) {
							StartCoroutine(TestMissile(hand));
							StartCoroutine (TestUpgrade(hand)); 
						}
						Test2HandMissle (hand, ref doubleHandMissleCastRightH);
						ShieldActive (hand);
						ChargingActive(hand);
					} else if (hand.IsLeft) {
						//TestMissle (hand);
						if (gameMode_test_isActive == false){
							StartCoroutine (SmallMissiles (hand)); // small missiles Offensive Actions
							StartCoroutine(Upgrades(hand)); // upgrades, Deffensive actions
						}
						if (gameMode_test_isActive == true) {
							StartCoroutine(TestMissile(hand));
							StartCoroutine (TestUpgrade(hand)); 
						}
						Test2HandMissle (hand, ref doubleHandMissleCastLeftH);
						ShieldActive (hand);
						ChargingActive(hand);
						//bool testCon = FingAnglesDet (hand, 30.0f, 0.0f, 0.0f, 0.0f);
						//bool testCon1 =FingProximalAngleDet (hand, 45.0f, 0.0f, 0.0f, 0.0f, 0.0f);
						//bool testCon3 = FingDistInterToProximalAngleDet (hand, 89.0f, 90.0f, 90.0f, 90.0f, 90.0f);
					}

				}
				// 2 hands gestures checks
				// 2 Hand Missle check
				if (gameMode_test_isActive == true) {
					StartCoroutine (Test2HandMissleCast ());
				}
				StartCoroutine (LargeMissiles());
				yield return new WaitForSeconds (Period);
			}
		}

		void Red(Hand cHand)
		{
			//firePoseActive = FirePose (cHand);
			firePoseActive = RedPose (cHand);
			if ( firePoseActive == true) {/*Debug.Log ("Red Fire Pose is active");*/
				tRed.GetComponent<TrajectoryRed> ().ActivateTra ();
			} else { /*Debug.Log ("Red Fire Pose NOT active");*/
				tRed.GetComponent<TrajectoryRed> ().DeactivateTra ();
			}
		}

		void Yellow(Hand cHand)
		{
			airPoseActive = YellowPose(cHand);
			if ( airPoseActive == true) {/*Debug.Log ("Air Yellow Pose is active");*/
				tYellow.GetComponent<TrajectoryYellow> ().ActivateTra ();
			} else { /*Debug.Log ("Air Yellow Pose NOT active");*/
				tYellow.GetComponent<TrajectoryYellow> ().DeactivateTra ();
			}
		}

		void Violet(Hand cHand)
		{
			aetherPoseActive = VioletPose(cHand);
			if ( aetherPoseActive == true) {/*Debug.Log ("Aether Violet Pose is active");*/
				tViolet.GetComponent<TrajectoryViolet> ().ActivateTra ();
			} else { /*Debug.Log ("Aether Violet Pose NOT active");*/
				tViolet.GetComponent<TrajectoryViolet> ().DeactivateTra ();
			}
		}

		void Brown(Hand cHand)
		{
			earthPoseActive = BrownPose(cHand);
			if ( earthPoseActive == true) {/*Debug.Log ("Brown Earth Pose is active");*/
				tBrown.GetComponent<TrajectoryBrown> ().ActivateTra ();
			} else { /*Debug.Log ("Earth Brown Pose NOT active");*/
				tBrown.GetComponent<TrajectoryBrown> ().DeactivateTra ();
			}
		}

		void Blue(Hand cHand)
		{
			waterPoseActive = BluePose(cHand);
			if ( waterPoseActive == true) {/*Debug.Log ("Blue Water Pose is active");*/
				tBlue.GetComponent<TrajectoryBlue> ().ActivateTra ();
			} else { /*Debug.Log ("Blue Water Pose NOT active");*/
				tBlue.GetComponent<TrajectoryBlue> ().DeactivateTra ();
			}
		}

		void Charging(Hand cHand)
		{
			PowerChargingOneHand (cHand);
		}


		void ShieldActive(Hand cHand)
		{
			if (ShieldPoseOneHand (cHand) == true) {
				//Debug.Log (" SHIELD ACTIVE ");



				if (cHand.IsLeft) {
					canvasLeft.GetComponentInChildren<Text> ().text = "SHIELD";
					shieldPoseHandLeft = true;
					//Gesture Timer for testing Quantity START
					shieldTimeQL = shieldTimeQL + Time.deltaTime;
					//Gesture Timer for testing Quantity END
				} else if (cHand.IsRight) {
					canvasRight.GetComponentInChildren<Text> ().text = "SHIELD";
					shieldPoseHandRight = true;
					//Gesture Timer for testing Quantity START
					shieldTimeQR = shieldTimeQR + Time.deltaTime;
					//Gesture Timer for testing Quantity END
				}
			} else if (ShieldPoseOneHand(cHand) == false){
				if (cHand.IsLeft) {
					if (powerChargingLeft == false) {
						canvasLeft.GetComponentInChildren<Text> ().text = "";
					}
					shieldPoseHandLeft = false;
				} else if (cHand.IsRight) {
					if (powerChargingRight == false) {
						canvasRight.GetComponentInChildren<Text> ().text = "";
					}
					shieldPoseHandRight = false;
				}
			}
		}

		void ChargingActive(Hand cHand)
		{
			if (PowerChargingOneHand(cHand) == true) {
				//Debug.Log (" Charging ACTIVE ");

				if (cHand.IsLeft) {
					canvasLeft.GetComponentInChildren<Text> ().text = "Charging";
					powerChargingLeft = true;
					//Gesture Timer for testing Quantity START
					chargingTimeQL = chargingTimeQL + Time.deltaTime;
					//Gesture Timer for testing Quantity END
				} else if (cHand.IsRight) {
					canvasRight.GetComponentInChildren<Text> ().text = "Charging";
					powerChargingRight = true;
					//Gesture Timer for testing Quantity START
					chargingTimeQR = chargingTimeQR + Time.deltaTime;
					//Gesture Timer for testing Quantity END
				}
			} else if (PowerChargingOneHand(cHand) == false){
				if (cHand.IsLeft) {
					if (shieldPoseHandLeft == false) {
						canvasLeft.GetComponentInChildren<Text> ().text = "";
					}
					powerChargingLeft = false;
				} else if (cHand.IsRight) {
					if (shieldPoseHandRight == false) {
						canvasRight.GetComponentInChildren<Text> ().text = "";
					}
					powerChargingRight = false;
				}
			}
		}
			


		void Test2HandMissle(Hand cHand, ref bool handCheck)
		{
			bool c1 = OneHandMissleCastForward (cHand);
			if (c1) {
				handCheck = true;
			} else if (c1 == false) {
				handCheck = false;
			}
		}

		IEnumerator Test2HandMissleCast(){
			if (doubleHandMissleCastLeftH && doubleHandMissleCastRightH && doubleHandCast) {
				doubleHandCast = false;
				Debug.Log ("Test 2HMissle ACTIVE");
				CanvasInstantiate ("TEST 2HMISSLE MSG", Color.black);
				yield return new WaitForSeconds (0.5f);
				doubleHandCast = true;
			}
		}

		IEnumerator TestMissile(Hand cHand)
		{
			//bool c1 = OneHandMissleCastForward (cHand);
			bool c2 = OneHandMissleCastSide (cHand);
			if ((/*c1 ||*/ c2) && oneHandCast == true) {
				oneHandCast = false;
				Debug.Log ("Test Missle ACTIVE");
				CanvasInstantiate ("TEST MISSLE MSG", Color.black);
				yield return new WaitForSeconds (0.5f);
				oneHandCast = true;
			}
		}

		IEnumerator TestUpgrade(Hand cHand)
		{
			bool c1 = UpgradeGesture (cHand);
			if (c1 && upgrade==true) {
				upgrade = false;
				Debug.Log ("UPGRADE ACTIVE");
				CanvasInstantiate ("TEST UPGRADE MSG", Color.black);
				yield return new WaitForSeconds (0.5f);
				upgrade = true;
			}

		}


		void UpgradesBase()
		{ 
			float EP = player.GetComponent<Player> ().EP;
			string msgU = "";
			Color colorU = Color.white;

			if (Signs [0] == (int)Elements.FIRE && Signs [1] == (int)Elements.NONE) {

				if (Tut_actions_check (18) && EP >= upgradeEPCost ) {
					Tut_action_set (19,true);
				}
				// red sign upgrade = Blue Shield
				player.GetComponent<Player>().SetShield(Shield.Red);
				//CanvasInstantiate("Test Red Shield",Color.red);
				msgU ="Test Red Shield";
				colorU = Color.red;
			

			} else if (Signs [0] == (int)Elements.WATER && Signs [1] == (int)Elements.NONE) {

				if (Tut_actions_check (19) && EP >= upgradeEPCost) {
					Tut_action_set (20,true);
				}
				// blue sign upgrade = Red Shield
				player.GetComponent<Player>().SetShield(Shield.Blue);
				//CanvasInstantiate("Test Blue Shield",Color.blue);
				msgU ="Test Blue Shield";
				colorU = Color.blue;


			} else if (Signs [0] == (int)Elements.AIR && Signs [1] == (int)Elements.NONE) {

				if (Tut_actions_check (20) && EP >= upgradeEPCost) {
					Tut_action_set (21,true);
				}
				// yellow sign upgrade = Brown Shield
				player.GetComponent<Player>().SetShield(Shield.Yellow);
				//CanvasInstantiate("Test Yellow Shield", Color.yellow);
				msgU ="Test Yellow Shield";
				colorU = Color.yellow;

			} else if (Signs [0] == (int)Elements.EARTH && Signs [1] == (int)Elements.NONE) {

				if (Tut_actions_check (21) && EP >= upgradeEPCost) {
					Tut_action_set (22,true);
				}
				//CanvasInstantiate("Test Brown Shield", brownColor);
				msgU ="Test Brown Shield";
				colorU = brownColor;

				// brown sign upgrade = Yellow Shield
				player.GetComponent<Player>().SetShield(Shield.Brown);

			} else if( Signs[0] == (int)Elements.EARTH && Signs[1] ==(int)Elements.AETHER && Signs[2] == (int)Elements.NONE){
				// kinetic shield upgrade, shield stat restore
				player.GetComponent<Player>().ShieldAdd(100.0f);
				//CanvasInstantiate("Test Kinetic Shield", Color.white);
				msgU ="Test Kinetic Shield";
				colorU = Color.white;
			} else if (Signs [0] == (int)Elements.NONE) {
				// no upgrade casted
				//CanvasInstantiate ("No Element to Cast", Color.white);

					msgU = "No Element to Cast";
					colorU = Color.white;
					CanvasInstantiate (msgU, colorU);
				if (Tut_actions_check (1)) {
					Tut_action_set (2, true);
				}
			} else {
				CanvasInstantiate (" Bad Combination ", Color.white);
				msgU = " Bad Combination ";
				colorU = Color.white;
				CanvasInstantiate (msgU, colorU);
				GameObject.Find ("Player").GetComponent<Player> ().AddDamage (10.0f, Color.white);
			}
			if (gameMode_test_isActive == true) {
				CanvasInstantiate (msgU, colorU);
			}

			SignsReset();
		}

		// Upgrades Defensive Actions
		IEnumerator Upgrades(Hand cHand)
		{
			bool c1 = UpgradeGesture (cHand);
			if (c1 && upgrade==true) {
				upgrade = false;
				//Gesture Quantity testingSTART
				upgradeQ++;
				////Gesture Quantity testing END
				UpgradesBase ();
				yield return new WaitForSeconds (0.5f);
				upgrade = true;
			}
		}


		void LargeMissilesBase()
		{
			float EP = player.GetComponent<Player> ().EP;
			//large missiles
			if (Signs [0] == (int)Elements.FIRE && Signs [1] == (int)Elements.AETHER && Signs [2] == (int)Elements.NONE) {
				if (Tut_actions_check (14)&& EP >= bigMissileEPCost) {
					Tut_action_set (15,true);
				}
					//cast red missile
					MissileSpawn (Target.Enemy, speedLargeMissile, MissileSize.H2, MissileType.RED);
			
			} else if (Signs [0] == (int)Elements.WATER && Signs [1] == (int)Elements.AETHER && Signs [2] == (int)Elements.NONE) {
				if (Tut_actions_check (15) && EP >= bigMissileEPCost) {
					Tut_action_set (16,true);
				}
					// cast blue missile
					MissileSpawn (Target.Enemy, speedLargeMissile, MissileSize.H2, MissileType.BLUE);

			} else if (Signs [0] == (int)Elements.AIR && Signs [1] == (int)Elements.AETHER && Signs [2] == (int)Elements.NONE) {
				if (Tut_actions_check (16) && EP >= bigMissileEPCost) {
					Tut_action_set (17,true);
				}
					//cast yellow missile
					MissileSpawn (Target.Enemy, speedLargeMissile, MissileSize.H2, MissileType.YELLOW);

			} else if (Signs [0] == (int)Elements.EARTH && Signs [1] == (int)Elements.AETHER && Signs [2] == (int)Elements.NONE) {
				if (Tut_actions_check (17) && EP >= bigMissileEPCost) {
					Tut_action_set (18,true);
				}
					//cast brown missile
					MissileSpawn (Target.Enemy, speedLargeMissile, MissileSize.H2, MissileType.BROWN);

			} else if (Signs [0] == (int)Elements.NONE) {
				// no missile casted

					CanvasInstantiate ("No Element to Cast", Color.white);
				if (Tut_actions_check (2)) {
					Tut_action_set (3, true);
				}
			} else {
				CanvasInstantiate (" Bad Combination ", Color.white);
				GameObject.Find ("Player").GetComponent<Player> ().AddDamage (10.0f, Color.white);
			}

			SignsReset();
		}

		//Large missile Offensive Actions
		IEnumerator LargeMissiles()
		{
			if (doubleHandMissleCastLeftH && doubleHandMissleCastRightH && doubleHandCast ) {
				doubleHandCast = false;

				/*
				 doubleHandCast = false;
				Debug.Log ("Test 2HMissle ACTIVE");
				CanvasInstantiate ("TEST 2HMISSLE MSG", Color.black);
				yield return new WaitForSeconds (0.5f);
				doubleHandCast = true;
				*/
				//Gesture Quantity testingSTART
				twoHCastQ++;
				////Gesture Quantity testing END
				LargeMissilesBase();
				yield return new WaitForSeconds (0.5f);
				doubleHandCast = true;
			}
		}


		void SmallMissilesBase()
		{
			float EP = player.GetComponent<Player> ().EP;
			if (Signs [0] == (int)Elements.FIRE && Signs [1] == (int)Elements.NONE) {
				if (Tut_actions_check (10) && EP >= smallMissileEPCost ) {
					Tut_action_set (11, true);
				}
					//cast red missile
					MissileSpawn (Target.Enemy, speedSmallMissile, MissileSize.H1, MissileType.RED);

			} else if (Signs [0] == (int)Elements.WATER && Signs [1] == (int)Elements.NONE) {
				if (Tut_actions_check (11) && EP >= smallMissileEPCost) {
					Tut_action_set (12, true);
				}
					// cast blue missile
					MissileSpawn (Target.Enemy, speedSmallMissile, MissileSize.H1, MissileType.BLUE);

			} else if (Signs [0] == (int)Elements.AIR && Signs [1] == (int)Elements.NONE) {
				if (Tut_actions_check (12) && EP >= smallMissileEPCost) {
					Tut_action_set (13, true);
				}
					//cast yellow missile
					MissileSpawn (Target.Enemy, speedSmallMissile, MissileSize.H1, MissileType.YELLOW);

			} else if (Signs [0] == (int)Elements.EARTH && Signs [1] == (int)Elements.NONE) {
				if (Tut_actions_check (13) && EP >= smallMissileEPCost) {
					Tut_action_set (14, true);
				}
					//cast brown missile
					MissileSpawn (Target.Enemy, speedSmallMissile, MissileSize.H1, MissileType.BROWN);
			
			} else if (Signs [0] == (int)Elements.NONE) {
				
					// no missile casted
					CanvasInstantiate ("No Element to Cast", Color.white);
				if (Tut_actions_check(0))
				{
					Tut_action_set (1, true);
				}

			} else {
				CanvasInstantiate (" Bad Combination ", Color.white);
				GameObject.Find ("Player").GetComponent<Player> ().AddDamage (10.0f, Color.white);
			}
			SignsReset();
		}

		//Small missile Offensive Actions
		IEnumerator SmallMissiles(Hand cHand)
		{
			//add  oneHandMissileCast code from oneHandMissileTest
			bool c1 = OneHandMissleCastSide(cHand);
			if (oneHandCast == true && c1 == true ) {
				oneHandCast = false;
				//small missiles
				//Gesture Quantity testingSTART
				oneHCastQ++;
				////Gesture Quantity testing END

				SmallMissilesBase ();

				yield return new WaitForSeconds (0.5f);
				oneHandCast = true;
			}
		}



		void GestureQuantityCanvasSet()
		{
			canvasGQ = GameObject.Find ("Canvas_GQ");
			if (gameMode_gestureQ_isActive == true) {
				canvasGQ.GetComponentInChildren<Text>().enabled = true;
			}
			if (gameMode_gestureQ_isActive == false) {
				canvasGQ.GetComponentInChildren<Text>().text = "";
			}
		}

		void GestureQuantityCanvasUpdate()
		{
			if (gameMode_gestureQ_isActive == true) {
				canvasGQ.GetComponentInChildren<Text> ().text = 
					"r: " + redQ + " bl: " + blueQ + " y: " + yellowQ + " br: " + brownQ + " a: " + violetQ + "\n"
					+ " 1H: " + oneHCastQ + " 2H: " + twoHCastQ + " U: " + upgradeQ + "\n"
					+ " CR: " + (int)chargingTimeQR + " CL: " + (int)chargingTimeQL + "\n"
					+ " SR: " + (int)shieldTimeQR + " SL: " + (int)shieldTimeQL;
			} 
		}

		public static void WinLoseCanvasSet(WinLose wl)
		{
			GameObject canvas = GameObject.Find ("Canvas_WinLose");
			if (wl == WinLose.Win) {
				scoreWIN++;
			}
			if (wl == WinLose.Lose) {
				scoreLose++;
			}
			if (wl == WinLose.Start)
			{
				scoreLose = 0;
				scoreWIN = 0;
			}
			canvas.GetComponentInChildren<Text>().text = " WIN: "+scoreWIN+ " LOSE: "+ scoreLose;
		}
		public static void DestroyMissiles()
		{
			GameObject[] missiles;
			missiles = GameObject.FindGameObjectsWithTag ("Missile");
			foreach (GameObject mis in missiles) {
				Destroy (mis);
			}

		}

		public void PadActionsUpdate()
		{
			if (PadCanvas.brownAction) { SignsAdd(Elements.EARTH) ;
				if (GameMaster.Tut_actions_check (7)) {
					GameMaster.Tut_action_set (8,true);
				}
			}
			if (PadCanvas.yellowAction) { SignsAdd(Elements.AIR) ;
				if (GameMaster.Tut_actions_check (5)) {
					GameMaster.Tut_action_set (6,true);
				}
			}
			if (PadCanvas.redAction) { SignsAdd(Elements.FIRE) ;
				if (GameMaster.Tut_actions_check (3)) {
					GameMaster.Tut_action_set (4,true);
				}
			}
			if (PadCanvas.blueAction) { SignsAdd(Elements.WATER) ;
				if (GameMaster.Tut_actions_check (4)) {
					GameMaster.Tut_action_set (5,true);
				}
			}
			if (PadCanvas.violletAction) { SignsAdd(Elements.AETHER) ;
				if (GameMaster.Tut_actions_check (6)) {
					GameMaster.Tut_action_set (7,true);
				}
			}

		}

		//Small missile Offensive Actions
		IEnumerator SmallMissilesPad()
		{
			if (oneHandCast == true && PadCanvas.smallCastAction == true ) {
				oneHandCast = false;
				//small missiles
				//Gesture Quantity testingSTART
				//oneHCastQ++;
				////Gesture Quantity testing END
				/// 
				SmallMissilesBase();

				yield return new WaitForSeconds (0.5f);
				oneHandCast = true;
			}
		}

		//Large missile Offensive Actions
		IEnumerator LargeMissilesPad()
		{
			if (PadCanvas.bigCastAction && doubleHandCast ) {
				doubleHandCast = false;

				LargeMissilesBase ();
			
				yield return new WaitForSeconds (0.5f);
				doubleHandCast = true;
			}
		}

		// Upgrades Defensive Actions
		IEnumerator UpgradesPad()
		{
			
			if (PadCanvas.upgradeAction && upgrade==true) {
				upgrade = false;
				//Gesture Quantity testingSTART
				//upgradeQ++;
				////Gesture Quantity testing END
				UpgradesBase();
				yield return new WaitForSeconds (0.5f);
				upgrade = true;
			}
		}

		void ChargingPad()
		{
			if ( PadCanvas.energyAction == true) {
				

					canvasLeft.GetComponentInChildren<Text> ().text = "Charging";
					powerChargingLeft = true;

					canvasRight.GetComponentInChildren<Text> ().text = "Charging";
					powerChargingRight = true;
					

			} else if (PadCanvas.energyAction == false  ){
				
				if (shieldPoseHandLeft == false) {
					canvasLeft.GetComponentInChildren<Text> ().text = "";
				}
					powerChargingLeft = false;

				if (shieldPoseHandRight == false) {
					canvasRight.GetComponentInChildren<Text> ().text = "";
				}
					powerChargingRight = false;

			}
		}

		void ShieldActivePad()
		{
			if (PadCanvas.shieldAction == true) {
				
					canvasLeft.GetComponentInChildren<Text> ().text = "SHIELD";
					shieldPoseHandLeft = true;


					canvasRight.GetComponentInChildren<Text> ().text = "SHIELD";
					shieldPoseHandRight = true;


			} else if (PadCanvas.shieldAction  == false  ){
				
				if (powerChargingLeft == false) {	
					canvasLeft.GetComponentInChildren<Text> ().text = "";
				}
					shieldPoseHandLeft = false;
				
				if (powerChargingRight == false) {	
					canvasRight.GetComponentInChildren<Text> ().text = "";
				}
					shieldPoseHandRight = false;

			}
		}

	}
}