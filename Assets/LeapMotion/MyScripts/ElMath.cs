﻿using UnityEngine;
using System.Collections;

public struct ElMath{

	public enum Mode {Mode2D, Mode3D, ModeVR};

	public static float DFromLine (Vector3 linePointS /*On line*/, Vector3 pointP /*Point away form the Line*/, Vector3 lineVecStart, Vector3 lineVecEnd)
	{
		//just 3D mode with no Mode atribute
		Vector3 lineVector = lineVecEnd - lineVecStart;
		return LengthFromVector (Vector3.Cross (pointP - linePointS, lineVector) / LengthFromVector (lineVector));
	}

	public static float DFromLine ( Mode mode ,Vector3 linePointS /*On line*/, Vector3 pointP /*Point away form the Line*/, Vector3 lineVecStart, Vector3 lineVecEnd)
	{
		Vector3 lineVector = lineVecEnd - lineVecStart;
		switch (mode) 
		{
		case Mode.Mode2D: 
			//instruction
			return LengthFromVector (Vector3.Cross (pointP - linePointS, lineVector), Mode.Mode2D) / LengthFromVector (lineVector, Mode.Mode2D) ;
			break;
		case Mode.Mode3D:
			//instruction
			return LengthFromVector (Vector3.Cross (pointP - linePointS, lineVector), Mode.Mode3D) / LengthFromVector (lineVector, Mode.Mode3D);
			break;
		case Mode.ModeVR:
			lineVector = Vector3.ProjectOnPlane (lineVector, Camera.main.transform.forward);
			Vector3 pointsVec = pointP - linePointS;
			pointsVec = Vector3.ProjectOnPlane (pointsVec, Camera.main.transform.forward);
			return LengthFromVector (Vector3.Cross (pointsVec, lineVector), Mode.Mode3D) / LengthFromVector (lineVector, Mode.Mode3D);
			break;
		default:
			return 0;
			break;
		}
	}

	public  static float DFromPlane( Vector3 normalVector, Vector3 pointP, Vector3 planePointS)
	{	
		//just 3D test is enough
		float alfa = -Vector3.Dot (normalVector, planePointS);
		return  (Vector3.Dot(normalVector, pointP) + alfa) / LengthFromVector(normalVector) ;
	}

	public static float LengthFromVector(Vector3 vec)
	{

		return Mathf.Sqrt(Mathf.Pow(vec.x,2)+Mathf.Pow(vec.y,2)+Mathf.Pow(vec.z,2));
	}

	public static float LengthFromVector(Vector3 vec, Mode mode)
	{

		switch (mode) {
		case Mode.Mode2D:
			return Mathf.Sqrt(Mathf.Pow(vec.x,2)+Mathf.Pow(vec.y,2));;
			break;
		case Mode.Mode3D:
			return Mathf.Sqrt (Mathf.Pow (vec.x, 2) + Mathf.Pow (vec.y, 2) + Mathf.Pow (vec.z, 2));
			break;
		case Mode.ModeVR:
			// get instruction
			Vector3 vOnCamPro;
			vOnCamPro = Vector3.ProjectOnPlane (vec, Camera.main.transform.forward);
			return Mathf.Sqrt (Mathf.Pow (vOnCamPro.x, 2) + Mathf.Pow (vOnCamPro.y, 2) + Mathf.Pow (vOnCamPro.z, 2));
			break;
		default:
			return 0;
			break;
		}

	}

	public static Vector3 MultiplyMatrixVector3(float[,] matrix3x3 , Vector3 vector3 ){

		Vector3 returnVector3;
		// Vector[0-2] zamiast x y z
		returnVector3.x = matrix3x3 [0, 0] * vector3.x + matrix3x3 [0, 1] * vector3.y + matrix3x3 [0, 2] * vector3.z;
		returnVector3.y = matrix3x3 [1, 0] * vector3.x + matrix3x3 [1, 1] * vector3.y + matrix3x3 [1, 2] * vector3.z;
		returnVector3.z = matrix3x3 [2, 0] * vector3.x + matrix3x3 [2, 1] * vector3.y + matrix3x3 [2, 2] * vector3.z;

		return returnVector3;
	}

	public static float[,] MultiplyMatrixScalar(float[,] matrix, float scalar){

		float[,] returnMatrix = new float[3,3];

		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				returnMatrix [i, j] = matrix [i, j] * scalar;
			}
		}
		return returnMatrix;
	}

	public static Vector3 RotOnAxis(Vector3 vector, Vector3 axis, float rotAngle)
	{
		//degrees
		rotAngle = rotAngle * Mathf.PI / 180;
		axis = axis / LengthFromVector (axis);
		float[,] RotOnAxis_Matrix = {
			{ Mathf.Pow(axis.x,2.0f)*(1.0f-Mathf.Cos(rotAngle))+Mathf.Cos(rotAngle), axis.x*axis.y*(1.0f-Mathf.Cos(rotAngle))+axis.z*Mathf.Sin(rotAngle) , axis.x*axis.z*(1.0f - Mathf.Cos(rotAngle)) - axis.y*Mathf.Sin(rotAngle)  },
			{ axis.x*axis.y*(1.0f-Mathf.Cos(rotAngle))-axis.z*Mathf.Sin(rotAngle), Mathf.Pow(axis.y,2.0f)*(1.0f-Mathf.Cos(rotAngle))+Mathf.Cos(rotAngle), axis.y*axis.z*(1.0f-Mathf.Cos(rotAngle))+axis.x*Mathf.Sin(rotAngle) }, 
			{ axis.x*axis.z*(1.0f-Mathf.Cos(rotAngle))+axis.y*Mathf.Sin(rotAngle), axis.y*axis.z*(1.0f-Mathf.Cos(rotAngle)) - axis.x*Mathf.Sin(rotAngle), Mathf.Pow(axis.z,2.0f)*(1.0f-Mathf.Cos(rotAngle))+Mathf.Cos(rotAngle) }

		};

		return MultiplyMatrixVector3 (RotOnAxis_Matrix, vector);

	}

	// Project vector on plane using planeVector which is perpendicular to the plane.
	public static Vector3 ProjectOnPlane(Vector3 vector, Vector3 planeVector)
	{
		//make a unit vector from plane Vector
		planeVector = planeVector / LengthFromVector(planeVector);
		//3D matrix to project vector onto an arbitrary plane
		float[,] TransformationMatrix = {
			{ 1.0f - Mathf.Pow(planeVector.x, 2.0f), -1.0f*planeVector.x*planeVector.y, -1.0f*planeVector.x*planeVector.z },
			{ -1.0f*planeVector.x*planeVector.y, 1.0f - Mathf.Pow( planeVector.y, 2.0f) , -1.0f*planeVector.y*planeVector.z }, 
			{ -1*planeVector.x * planeVector.z, -1.0f*planeVector.y*planeVector.z, 1.0f - Mathf.Pow(planeVector.z,2.0f) }

		};

		return MultiplyMatrixVector3 (TransformationMatrix, vector);
	}

	// Project vector on n vector
	public static Vector3 Project(Vector3 vector, Vector3 n  )
	{
		Vector3 rVector;
		float s = 1.0f / (LengthFromVector (n) * LengthFromVector (n));
		float[,] TransformationMatrix = {
			{ Mathf.Pow(n.x, 2.0f), n.x*n.y, n.x*n.z },
			{ n.x*n.y, Mathf.Pow( n.y, 2.0f) , n.y*n.z }, 
			{ n.x * n.z, n.y*n.z, Mathf.Pow(n.z,2.0f) }
		};
		TransformationMatrix = MultiplyMatrixScalar (TransformationMatrix, s);
		rVector = MultiplyMatrixVector3 (TransformationMatrix, vector);
		return rVector;
	}

}



