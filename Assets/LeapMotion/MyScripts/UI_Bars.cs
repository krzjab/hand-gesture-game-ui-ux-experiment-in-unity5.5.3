﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//add to Manager_UI Object in game scene

public class UI_Bars : MonoBehaviour {

	GameObject player;
	GameObject enemy;

	float panelWidthMax = 333.0f;
	float panelHeight = 30.0f;

	GameObject panel_hp;
	GameObject panel_sp;
	GameObject panel_ep;
	GameObject panel_enemy_hp;

	float hp = 0.0f;
	float hpMax = 0.0f;
	float sp = 0.0f;
	float spMax = 0.0f;
	float ep = 0.0f;
	float epMax = 0.0f;
	float enemyHp = 0.0f;
	float enemyHpMax = 0.0f;

	// Use this for initialization
	void Start () {
		float panelWidth = panelWidthMax;
		float hpWidth = panelWidthMax;
		float spWidth = panelWidthMax;
		float epWidth = panelWidthMax;

		player = GameObject.Find ("Player");
		enemy = GameObject.Find ("Enemy");
		panel_hp = GameObject.Find ("Panel_HP/Value");
		panel_sp = GameObject.Find ("Panel_SP/Value");
		panel_ep = GameObject.Find ("Panel_EP/Value");

		panel_enemy_hp = GameObject.Find ("Panel_Enemy_HP/Value");
	}
	
	// Update is called once per frame
	void Update () {
		hp = player.GetComponent<Player> ().HP;
		hpMax = player.GetComponent<Player> ().HPMax;
		sp = player.GetComponent<Player> ().SP;
		spMax = player.GetComponent<Player> ().SPMax;
		ep = player.GetComponent<Player> ().EP;
		epMax = player.GetComponent<Player> ().EPMax;
		enemyHp = enemy.GetComponent<EnemyBasic> ().HP;
		enemyHpMax = enemy.GetComponent<EnemyBasic> ().HPMax;

		//RectTransform rt = panel_hp.GetComponent<RectTransform> ();
		//rt.sizeDelta = new Vector2( panelWidthMax * hp / hpMax , panelHeight );
		panel_hp.GetComponent<RectTransform>().sizeDelta = new Vector2( panelWidthMax * hp / hpMax, panelHeight ) ;
		panel_sp.GetComponent<RectTransform>().sizeDelta = new Vector2( panelWidthMax * sp / spMax, panelHeight ) ;
		panel_ep.GetComponent<RectTransform>().sizeDelta = new Vector2( panelWidthMax * ep / epMax, panelHeight ) ;

		panel_enemy_hp.GetComponent<RectTransform>().sizeDelta = new Vector2( panelWidthMax * enemyHp / enemyHpMax, panelHeight ) ;

	}
}
