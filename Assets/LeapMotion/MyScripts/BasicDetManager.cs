﻿using UnityEngine;
using System.Collections;
using Leap;
using Leap.Unity;
using ElementsGame;

public class BasicDetManager : MonoBehaviour {

	static bool[] fingersEx = new bool[5] {false,false,false,false,false}; 
	enum FingerName {THUMB , INDEX, MIDDLE, RING, PINKY}

	protected bool FingersExDetect( Hand h , bool thumb, bool index, bool middle, bool ring, bool pinky)
	{
		bool[] fe = new bool[5] { thumb, index, middle, ring, pinky };
		int exCount = 0;// count to 5, if 5 then FingerExDetect is right with reality
		for (int i = 0; i < 5; i++ ) 
		{
			if (h.Fingers [i].IsExtended == fe [i]) 
			{
				exCount++;
			}
		}
		if (exCount == 5) {
			return true;
		} else {
			return false;
		}
	}

	protected bool DirectionDetect(Vector3 mainVector, Vector3 planeVector, Vector3 targetVector, float condAngAlpha, float condAngBeta)
	{
		Vector3 v_MainOnPlaneVec;
		float f_MainToPlaneAngle;
		float f_PlaneToDirectionAngle;
		bool con01;
		bool con02;
		v_MainOnPlaneVec = Vector3.ProjectOnPlane (mainVector, planeVector);
		f_MainToPlaneAngle = Vector3.Angle (mainVector, v_MainOnPlaneVec);
		f_PlaneToDirectionAngle = Vector3.Angle (v_MainOnPlaneVec, targetVector);

		if (f_MainToPlaneAngle <= condAngAlpha && f_MainToPlaneAngle >= -1*condAngAlpha) {
			con01 = true;
		} else {
			con01 = false;
		}
		if (f_PlaneToDirectionAngle <= condAngBeta && f_MainToPlaneAngle >= -1*condAngBeta) {
			con02 = true;
		} else {
			con02 = false;
		}
		if (con01 == true && con02 == true) {
			//Debug.Log ("DirectionCheck is working. Value is true.");
			return true;
		} else {
			//Debug.Log("DirectionCheck is working. Value is false.");
			return false;
		}
	}

	protected bool DirectionDetect(Vector3 mainVector, Vector3 targetVector, float condAngAlpha)
	{
		float f_Angle;
		bool con01;
		f_Angle = Vector3.Angle (mainVector, targetVector);
		if (f_Angle <= condAngAlpha && f_Angle >= -1*condAngAlpha) {
			con01 = true;
		} else {
			con01 = false;
		}
		if (con01 == true) {
			//Debug.Log ("DirectionCheck is working. Value is true.");
			return true;
		} else {
			//Debug.Log("DirectionCheck is working. Value is false.");
			return false;
		}
	}

	protected bool VelocityDet(Hand h, float velocityOnValue /*1.5f eg*/, Vector3 targetVector /*transform.forward right up etc*/)
	{
		Vector3 velocityV;
		velocityV = h.PalmVelocity.ToVector3 ();
		Vector3 velocityVPro;
		velocityVPro = Vector3.Project (velocityV, targetVector);
		float magnitude = ElMath.LengthFromVector (velocityVPro);
		bool digitTest;
		digitTest = DigitTest (velocityVPro, targetVector);
		if ((velocityOnValue <= magnitude) && digitTest) {
			Debug.Log ("Velocity Check is ON"+ " magnitude= "+ magnitude+ " velocityOnValue= "+ velocityOnValue + " direction.x= "+ targetVector.x + " direction.y= "+ targetVector.y+ " direction.z= "+ targetVector.z );
			return true;
		} else {
			return false;
		}

	}

	protected bool FingAnglesDet(Hand fingH, float angleTolerance, float thumbIndexA, float indexMiddleA, float middleRingA, float ringPinkyA)
	{
		int count = 0;
		float[] angleOnValue = new float[4] {thumbIndexA, indexMiddleA, middleRingA, ringPinkyA};
		float[] fingAngle = new float[4]; // finger to next finger angle, eg. angle from thumb to indexfinger
		Vector3[] fingerOnPlane = new Vector3[5]; // fingers thumb = 0, indexFinger = 1, ...
		for (int i = 0; i < 5; i++) {
			if (i == 0) {
				//for thumb
				fingerOnPlane [i] = ElMath.ProjectOnPlane (GameMaster.BonesLeapVector (fingH.Fingers [i].bones [3].NextJoint, fingH.Fingers [i].bones [2].PrevJoint).ToVector3 (), fingH.PalmNormal.ToVector3 ());
			} else if (i>0)
			{
				fingerOnPlane [i] = ElMath.ProjectOnPlane (GameMaster.BonesLeapVector (fingH.Fingers [i].bones [3].NextJoint, fingH.Fingers [i].bones [1].PrevJoint).ToVector3 (), fingH.PalmNormal.ToVector3 ());
			}
		}
		// get angles in degrees from v1 to v2
		for (int i = 0; i < 4; i++) {
			fingAngle [i] = Vector3.Angle (fingerOnPlane [i], fingerOnPlane [i + 1]);
		}
		//check angle values,  are they in angle tolerance?
		for (int i = 0; i < 4; i++) {
			if ((fingAngle [i] >= (angleOnValue [i] - angleTolerance / 2.0f)) && (fingAngle [i] <= (angleOnValue [i] + angleTolerance / 2.0f))) {
				count++;
			}
		}
		//debug.Log for angle values
		for (int i = 0; i < 4; i++) {
			//Debug.Log( "Angle between fingers " + i + " and  " + (i+1.0) + " is equal to " + fingAngle[i] + " Count= " + count);
		}
		if (count == 4)
			return true;
		else
			return false;
	}

	// fingers angles Detection without the thumb
	protected bool FingAnglesDet(Hand fingH, float angleTolerance, float indexMiddleA, float middleRingA, float ringPinkyA)
	{
		int count = 0;
		float[] angleOnValue = new float[3] {indexMiddleA, middleRingA, ringPinkyA};
		float[] fingAngle = new float[3]; // finger to next finger angle, eg. angle from thumb to indexfinger
		Vector3[] fingerOnPlane = new Vector3[4]; // fingers thumb = 0, indexFinger = 1, ...
		for (int i = 0; i < fingerOnPlane.Length; i++) {
			int fingerNr = i + 1;
			fingerOnPlane [i] = ElMath.ProjectOnPlane (GameMaster.BonesLeapVector (fingH.Fingers [fingerNr].bones [3].NextJoint, fingH.Fingers [fingerNr].bones [1].PrevJoint).ToVector3 (), fingH.PalmNormal.ToVector3 ());
		}
		// get angles in degrees from v1 to v2
		for (int i = 0; i < fingAngle.Length; i++) {
			fingAngle [i] = Vector3.Angle (fingerOnPlane [i], fingerOnPlane [i + 1]);
		}
		//check angle values,  are they in angle tolerance?
		for (int i = 0; i < fingAngle.Length; i++) {
			if ((fingAngle [i] >= (angleOnValue [i] - angleTolerance / 2.0f)) && (fingAngle [i] <= (angleOnValue [i] + angleTolerance / 2.0f))) {
				count++;
			}
		}
		//debug.Log for angle values
		for (int i = 0; i < fingAngle.Length; i++) {
			//Debug.Log( "Angle between fingers " + i + " and  " + (i+1.0) + " is equal to " + fingAngle[i] + " Count= " + count + " ToleranceAngle= "+ angleTolerance);
		}
		if (count == 3)
			return true;
		else
			return false;
	}

	//Check angles between fingers proximal bones and the hand direction vector on the sidePlane of Hand
	protected bool FingProximalAngleDet(Hand fingH, float angleTolerance, float indexA1, float middleA1, float ringA1, float pinkyA1)
	{
		int count = 0;
		Vector3 v_sidePlaneNormal = ElMath.RotOnAxis (fingH.PalmNormal.ToVector3 (), fingH.Direction.ToVector3 (), 90.0f);
		float[] angleOnValue = new float[4] {indexA1, middleA1, ringA1, pinkyA1};
		float[] proximalBoneAngle = new float[4];
		Vector3[] proximalBoneOnPlane = new Vector3[4]; // fingers thumb = 0, indexFinger = 1, ...
		for (int i = 0; i < 4; i++) {
			int fingerNr = i + 1;
			proximalBoneOnPlane [i] = ElMath.ProjectOnPlane (GameMaster.BonesLeapVector (fingH.Fingers [fingerNr].bones [1].NextJoint, fingH.Fingers [fingerNr].bones [1].PrevJoint).ToVector3 (), v_sidePlaneNormal);
		}
		// get angles in degrees from v1 to v2
		for (int i = 0; i < 4; i++) {
			proximalBoneAngle [i] = Vector3.Angle (proximalBoneOnPlane [i], fingH.Direction.ToVector3());
		}
		//check angle values,  are they in angle tolerance?
		for (int i = 0; i < 4; i++) {
			if ((proximalBoneAngle [i] >= (angleOnValue [i] - angleTolerance / 2.0f)) && (proximalBoneAngle [i] <= (angleOnValue [i] + angleTolerance / 2.0f))) {
				count++;
			}
		}
		//debug.Log for angle values
		for (int i = 0; i < 4; i++) {
			//Debug.Log( "Angle between fingers proximal bones and hand direction: finger number= " + i + " is equal to " + proximalBoneAngle[i] + " Count= " + count + " ToleranceAngle= "+ angleTolerance);
		}

		if (count == 4)
			return true;
		else
			return false;

	}

	//Check angles between fingers [V1= distalBoneTip - intermediateBoneBase] 
	//and [V2= proximalTip - proximalBase]
	protected bool FingDistInterToProximalAngleDet(Hand fingH, float angleTolerance, 
		float indexB1,float middleB1,float ringB1, float pinkyB1)
	{
		int count = 0;
		Vector3 v_sidePlaneNormal = ElMath.RotOnAxis (fingH.PalmNormal.ToVector3 (),
			fingH.Direction.ToVector3 (), 90.0f);
		float[] angleOnValue = new float[4] {indexB1, middleB1, ringB1, pinkyB1};
		float[] distalInterToProxBoneAngle = new float[4];
		// fingers thumb = 0, indexFinger = 1, ...
		Vector3[] distalInterBonesOnPlane = new Vector3[4]; 
		Vector3[] proximalBoneOnPlane = new Vector3[4];
		for (int i = 0; i < 4; i++) {
			int fingerNr = i + 1;
			distalInterBonesOnPlane [i] = ElMath.ProjectOnPlane (
				GameMaster.BonesLeapVector (fingH.Fingers [fingerNr].bones [3].NextJoint,
				fingH.Fingers [fingerNr].bones [2].PrevJoint).ToVector3 (), v_sidePlaneNormal);
			proximalBoneOnPlane [i] = ElMath.ProjectOnPlane (
				GameMaster.BonesLeapVector (fingH.Fingers [fingerNr].bones [1].NextJoint,
					fingH.Fingers [fingerNr].bones [1].PrevJoint).ToVector3 (), v_sidePlaneNormal);
		}
		// get angles in degrees from v1 to v2
		for (int i = 0; i < 4; i++) {
			distalInterToProxBoneAngle [i] = Vector3.Angle (distalInterBonesOnPlane [i],
				proximalBoneOnPlane[i]);
		}
		//check angle values,  are they in angle tolerance?
		for (int i = 0; i < 4; i++) {
			if ((distalInterToProxBoneAngle [i] >= (angleOnValue [i] - angleTolerance / 2.0f)) 
				&& (distalInterToProxBoneAngle [i] <= (angleOnValue [i] + angleTolerance / 2.0f))) {
				count++;
			}
		}
		//debug.Log for angle values
		for (int i = 0; i < 4; i++) {
			/*Debug.Log( "Angle between fingers DistalInterBones and ProximalBone  finger number= "
			+ i + " angle is equal to " + distalInterToProxBoneAngle[i] + " Count= " + count +
			" ToleranceAngle= "+ angleTolerance);
			*/
		}
		if (count == 4)
			return true;
		else
			return false;

	}

	protected Vector3 PointProjectedOnAxis( Vector3 point, Vector3 originPoint, Vector3 axisVector )
	{
		Vector3 a = originPoint;
		Vector3 p = point;
		Vector3 result = a + Vector3.Dot (p - a, axisVector) / Vector3.Dot (axisVector, axisVector) * axisVector;
		return result;
	}

	public enum ScreenPos { onLEFT, onRIGHT, onTOP, onDOWN};

	// Palm Position Detector - checks where users hand is (on wich part of the screen)
	protected bool PalmPosDet( Hand sHand, ScreenPos hPos )
	{
		Vector3 p_pointOnAxisProjection;
		Vector3 v_originToProjectedPoint;
		switch (hPos)
		{
		case ScreenPos.onLEFT:
			p_pointOnAxisProjection = PointProjectedOnAxis (sHand.PalmPosition.ToVector3 (), Camera.main.transform.position, Camera.main.transform.right * (-1.0f));
			v_originToProjectedPoint = p_pointOnAxisProjection - Camera.main.transform.position;
			if ( DigitTest(v_originToProjectedPoint, Camera.main.transform.right*(-1.0f)) ) {
					return true;
				} else {
					return false;
				}
				break;
			case ScreenPos.onRIGHT:
				p_pointOnAxisProjection = PointProjectedOnAxis (sHand.PalmPosition.ToVector3 (), Camera.main.transform.position, Camera.main.transform.right);
				v_originToProjectedPoint = p_pointOnAxisProjection - Camera.main.transform.position;
				if ( DigitTest(v_originToProjectedPoint, Camera.main.transform.right) ) {
					return true;
				} else {
					return false;
				}
				break;
			case ScreenPos.onTOP:
				p_pointOnAxisProjection = PointProjectedOnAxis (sHand.PalmPosition.ToVector3 (), Camera.main.transform.position, Camera.main.transform.up);
				v_originToProjectedPoint = p_pointOnAxisProjection - Camera.main.transform.position;
				if ( DigitTest(v_originToProjectedPoint, Camera.main.transform.up) ) {
					return true;
				} else {
					return false;
				}
				break;
			case ScreenPos.onDOWN:
				p_pointOnAxisProjection = PointProjectedOnAxis (sHand.PalmPosition.ToVector3 (), Camera.main.transform.position, Camera.main.transform.up* (-1.0f));
				v_originToProjectedPoint = p_pointOnAxisProjection - Camera.main.transform.position;
				if ( DigitTest(v_originToProjectedPoint, Camera.main.transform.up* (-1.0f)) ) {
					return true;
				} else {
					return false;
				}
				break;
			default:
				return false;
				break;
		}
	}



	protected bool HandInView( Hand vHand)
	{
		if (vHand.TimeVisible > 0.01f && vHand != null) {
			return true;
		} else {
			return false;
		}
	}

	private bool DigitTest(Vector3 v1, Vector3 v2)
	{
		int count = 0;
		if ((v1.x > 0.0f && v2.x > 0.0f) || (v1.x == 0.0f && v2.x == 0.0f) || (v1.x < 0.0f && v2.x < 0.0f)) {
			count++;
		}  
		if ((v1.y > 0.0f && v2.y > 0.0f) || (v1.y == 0.0f && v2.y == 0.0f) || (v1.y < 0.0f && v2.y < 0.0f)) {
			count++;
		} 
		if ((v1.z > 0.0f && v2.z > 0.0f) || (v1.z == 0.0f && v2.z == 0.0f) || (v1.z < 0.0f && v2.z < 0.0f)) {
			count++;
		}
		if (count == 3) {
			return true;
		} else {
			return false;
		}
	}

	public static Vector BonesLeapVector( Vector vec1, Vector vec2)
	{
		return vec1 - vec2;
	}

}
