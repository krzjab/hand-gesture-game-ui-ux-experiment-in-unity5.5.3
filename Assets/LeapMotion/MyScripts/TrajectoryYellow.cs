﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Leap;
using Leap.Unity;
using UnityEngine.Events;
using UnityEngine.UI;
using ElementsGame;

public class TrajectoryYellow : TrajectoryBase {

	// Use this for initialization
	override protected void Start () {
		base.Start ();
		signMsg = "YELLOW SIGN COMPLETED";
		signTextColor = Color.yellow;
		signColor = GameMaster.Elements.AIR;

	}

	// Update is called once per frame
	void Update () {

	}

	void Subscribe()
	{
		//GameMaster.OnRed += TrajectoryCompleted;
	}

	void UnSubscribe()
	{
		//GameMaster.OnRed -= TrajectoryCompleted;
	}

	void TrajectoryCompleted()
	{

	}

	override protected void TrajectoryDone()
	{
		base.TrajectoryDone();
		//GameMaster.OnRed += RedMessage;
		Debug.Log ("YellowMessage added to OnYellow");
		YellowMessage ();
		// add message for gamemaster
		//RedMessage();
	}

	void YellowMessage()
	{

		Debug.Log("Yellow SIGN COMPLETED");
		//tutorial unlockings
		if (GameMaster.Tut_actions_check (5)) {
			GameMaster.Tut_action_set (6,true);
		}
	}


	override protected void SetCheckpoints()
	{
		cp = new GameObject[4];
		cp [0] = GameObject.Find ("SY1");
		cp [1] = GameObject.Find ("SY2");
		cp [2] = GameObject.Find ("SY3");
		cp [3] = GameObject.Find ("SY4");

	}

	override protected void SetMaterials()
	{
		whiteMat =  Resources.Load("Materials/White", typeof(Material)) as Material;
		signMat = Resources.Load("Materials/Yellow", typeof(Material)) as Material;
	}
}
