﻿using UnityEngine;
public class VolumeT  {
	
	public GameObject volumenObj;
	public bool isVisible;
	public bool isActive;
	public bool wasActive;

	public VolumeT (GameObject volume)
	{
		volumenObj = volume;
		isVisible = false;
		isActive = false;
		wasActive = false;
	}
	public VolumeT ()
	{
		volumenObj = null;
		isVisible = false;
		isActive = false;
		wasActive = false;
	}
}

//using System.Collections;
//using System;