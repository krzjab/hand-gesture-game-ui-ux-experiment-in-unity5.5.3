﻿using UnityEngine;
using System.Collections;
using Leap;
using Leap.Unity;

public enum Target {Enemy, Player};
public enum MissileSize { H1, H2};
public enum MissileType {RED, BLUE, YELLOW, BROWN};

public class MechanicsManager : GestureManager {

	public static Color brownColor = new Color(0.272f,0.171f,0.084f);
	public static Color violetColor =  new Color (0.85f,0.113f,0.937f);

	public static float smallMissileEPCost = 15.0f;
	public static float bigMissileEPCost = 25.0f;
	public static float upgradeEPCost = 10.0f;

// offensiv mechanics
// defensiv mechanics

	protected static GameObject playerSpawnPointCenter;
	protected static GameObject playerSpawnPointRight;
	protected static GameObject enemySpawnPoint;
	protected static GameObject enemy;
	protected static GameObject player;

	virtual protected void Start()
	{
		playerSpawnPointCenter = GameObject.Find ("PlayerMissleSpawn_Center");
		playerSpawnPointRight = GameObject.Find ("PlayerMissleSpawn_Right");
		enemySpawnPoint = GameObject.Find ("EnemyMissleSpawn");
		enemy = GameObject.Find ("Enemy");
		player = GameObject.Find ("Player");
	}
		

	public static void MissileSpawn( Target target, float speed, MissileSize ms, MissileType mt)
	{
		GameObject missile;
		Transform missileTarget;
		Transform missileSpawnPoint;
		string misslePrefabName;

		missileTarget = target == Target.Enemy ? enemy.transform : player.transform;
		if (missileTarget == enemy.transform) {
			missileSpawnPoint = ms == MissileSize.H1 ? playerSpawnPointRight.transform : playerSpawnPointCenter.transform;
		} else {
			missileSpawnPoint = enemySpawnPoint.transform;
		}

		misslePrefabName = ms == MissileSize.H1 ? "Missile1H" : "Missile2H";

		//MissileEnergyCost-------
		float energyCost=0;
		if (target == Target.Enemy) {
			if (MissileSize.H1 == ms)
				energyCost = smallMissileEPCost;
			if (MissileSize.H2 == ms)
				energyCost = bigMissileEPCost;
		}
		//MissileEnergyCost-------

		//Instantiate missile
		if (energyCost <= player.GetComponent<Player>().EP){
			missile = (GameObject)Instantiate(Resources.Load ("Prefabs/" + misslePrefabName, typeof(GameObject)), missileSpawnPoint.position, missileSpawnPoint.rotation);
			missile.GetComponent<Missile> ().missileSpeed = speed; //add speed to missle
			missile.GetComponent<Missile> ().missileTargetName = target == Target.Enemy ? enemy.name : player.name; // add target name
			player.GetComponent<Player>().EnergyCost(energyCost);

			// add color to missile
			if (mt == MissileType.RED)
				missile.GetComponent<Missile> ().missileColor = Color.red;
			else if (mt == MissileType.BLUE)
				missile.GetComponent<Missile> ().missileColor = Color.blue;
			else if (mt == MissileType.YELLOW)
				missile.GetComponent<Missile> ().missileColor = Color.yellow;
			else if (mt == MissileType.BROWN)
				missile.GetComponent<Missile> ().missileColor = brownColor; // brown color

			// add start and end position of spawned missile
			missile.GetComponent<Missile> ().startPos = missileSpawnPoint.position;
			missile.GetComponent<Missile> ().endPos = missileTarget.position;

			missile.GetComponent<Missile> ().missileSize = ms;
			missile.GetComponent<Missile> ().missileType = mt;

			//get missle script and add speed to it, add Element to missle (color and MissleType)

		}else{
			CanvasInstantiate ("Need " + energyCost + " energy", new Color (148.0f, 0.0f, 211.0f));
		}

	
	}



}
