﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Leap;
using Leap.Unity;
using UnityEngine.Events;
using UnityEngine.UI;
using ElementsGame;

public class TrajectoryViolet : TrajectoryBase {

	// Use this for initialization
	override protected void Start () {
		base.Start ();
		signMsg = "VIOLET SIGN COMPLETED";
		signTextColor = new Color (0.85f,0.113f,0.937f);
		signColor = GameMaster.Elements.AETHER;

	}

	// Update is called once per frame
	void Update () {

	}

	void Subscribe()
	{
		//GameMaster.OnRed += TrajectoryCompleted;
	}

	void UnSubscribe()
	{
		//GameMaster.OnRed -= TrajectoryCompleted;
	}

	void TrajectoryCompleted()
	{

	}

	override protected void TrajectoryDone()
	{
		base.TrajectoryDone();
		//GameMaster.OnRed += RedMessage;
		Debug.Log ("VioletMessage added to OnViolet");
		VioletMessage ();
		// add message for gamemaster
		//RedMessage();
	}

	void VioletMessage()
	{

		Debug.Log("Violet SIGN COMPLETED");
		//tutorial unlockings
		if (GameMaster.Tut_actions_check (6)) {
			GameMaster.Tut_action_set (7,true);
		}

	}


	override protected void SetCheckpoints()
	{
		cp = new GameObject[4];
		cp [0] = GameObject.Find ("SV1");
		cp [1] = GameObject.Find ("SV2");
		cp [2] = GameObject.Find ("SV3");
		cp [3] = GameObject.Find ("SV4");

	}

	override protected void SetMaterials()
	{
		whiteMat =  Resources.Load("Materials/White", typeof(Material)) as Material;
		signMat = Resources.Load("Materials/Violet", typeof(Material)) as Material;
	}
}
