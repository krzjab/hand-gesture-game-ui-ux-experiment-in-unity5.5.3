﻿using UnityEngine;
using System.Collections;
using Leap;
using Leap.Unity;
using UnityEngine.VR;
using UnityEngine.UI;

public class GestureManager : BasicDetManager {

	public enum Elements {NONE, FIRE, WATER, EARTH, AIR, AETHER}
	public enum WhichHand {LEFT_HAND , RIGHT_HAND}
	public static int[] Signs = new int[5] {(int)Elements.NONE, (int)Elements.NONE, (int)Elements.NONE, (int)Elements.NONE, (int)Elements.NONE};

	protected bool firePoseActive;
	protected bool waterPoseActive;
	protected bool earthPoseActive;
	protected bool airPoseActive;
	protected bool aetherPoseActive;

	//Gesture quantity - game testing START
	public int redQ = 0;
	public int blueQ = 0;
	public int yellowQ = 0;
	public int brownQ = 0;
	public int violetQ = 0;
	public int oneHCastQ = 0;
	public int twoHCastQ = 0;
	public int upgradeQ = 0;
	public float chargingTimeQL = 0.0f;
	public float chargingTimeQR = 0.0f;
	public float shieldTimeQL = 0.0f;
	public float shieldTimeQR = 0.0f;
	//Gesture quantity - game testing END
	//tutorial actions array for disable and enable them
	public static bool[] tut_actions = new bool[23];

	protected Sprite[] SlotsColor = new Sprite[6]; // colors (colored Sprites) for the signs interface slots
	protected GameObject[] SlotsGO = new GameObject[5]; // gameObjects of canvas for sign slots
	protected string SlotsNameBase = "Slot_"; // add number by iteration in SignsSet() method

	public static  void CanvasInstantiate(string msg, Color c)
	{
		GameObject canvasPanel;
		canvasPanel = (GameObject)Instantiate (Resources.Load ("Prefabs/Panel", typeof(GameObject)), GameObject.Find ("Canvas_Info").transform);
		canvasPanel.GetComponentInChildren<Text> ().text = msg;
		canvasPanel.GetComponentInChildren<Text> ().color = c;
		//Resources.Load("Materials/Violet", typeof(Material)) as Material;
	}

	public static  void CanvasInstantiatePlayer(string msg, Color c)
	{
		GameObject canvasPanel;
		canvasPanel = (GameObject)Instantiate (Resources.Load ("Prefabs/PanelLeft", typeof(GameObject)), GameObject.Find ("Canvas_Info").transform);
		canvasPanel.GetComponentInChildren<Text> ().text = "Player: " + msg;
		canvasPanel.GetComponentInChildren<Text> ().color = c;
	}

	static public void CanvasInstantiateEnemy(string msg, Color c)
	{
		GameObject canvasPanel;
		canvasPanel = (GameObject)Instantiate (Resources.Load ("Prefabs/PanelRight", typeof(GameObject)), GameObject.Find ("Canvas_Info").transform);
		canvasPanel.GetComponentInChildren<Text> ().text = "Enemy: " + msg;
		canvasPanel.GetComponentInChildren<Text> ().color = c;
	}

	public static void SignsReset()
	{
		for (int i=0; i<Signs.Length; i++) 
		{
			Signs[i] = (int) Elements.NONE;
		}
	}

	public void SignsAdd(Elements el)
	{
		if (el == Elements.NONE) {
			//nothing
		} else {
			for (int i = 0; i < Signs.Length; i++) {
				if (Signs [i] == (int)Elements.NONE) {
					
					Signs [i] = (int)el;
					//Gesture Quantity testingSTART
					if (el == Elements.FIRE)
						redQ++;
					if (el == Elements.WATER)
						blueQ++;
					if (el == Elements.AIR)
						yellowQ++;
					if (el == Elements.EARTH)
						brownQ++;
					if (el == Elements.AETHER)
						violetQ++;
					////Gesture Quantity testing END
					break;
				} else if (Signs [4] != (int)Elements.NONE) {
					Debug.Log ("Reset Signs AND Damage to Player");
					CanvasInstantiate("Reset Signs + Damage to Player", Color.black);
					SignsReset ();
					break;
				}
			}
		}
	}

	protected void SignsSet()
	{
		// Set Sign Slots - gameObjects for canvas
		// Set Sprites for Slots
		for (int i = 0; i < Signs.Length; i++) {
			SlotsGO [i] = GameObject.Find (SlotsNameBase + (i+1));
		}
		SlotsColor[(int)Elements.NONE] = Resources.Load("Sprites/slot", typeof(Sprite)) as Sprite;
		SlotsColor[(int)Elements.FIRE] = Resources.Load("Sprites/red", typeof(Sprite)) as Sprite;
		SlotsColor[(int)Elements.WATER] = Resources.Load("Sprites/blue", typeof(Sprite)) as Sprite;
		SlotsColor[(int)Elements.EARTH] = Resources.Load("Sprites/brown", typeof(Sprite)) as Sprite;
		SlotsColor[(int)Elements.AIR] = Resources.Load("Sprites/yellow", typeof(Sprite)) as Sprite;
		SlotsColor[(int)Elements.AETHER] = Resources.Load("Sprites/violet", typeof(Sprite)) as Sprite;

	}

	protected void SignsDraw()
	{
		for (int i = 0; i < SlotsGO.Length; i++) {
			if (Signs [i] == (int)Elements.NONE)
				SlotsGO [i].GetComponent<UnityEngine.UI.Image>().sprite = SlotsColor [(int)Elements.NONE];
			else if (Signs[i] == (int)Elements.FIRE)
				SlotsGO [i].GetComponent<UnityEngine.UI.Image>().sprite = SlotsColor [(int)Elements.FIRE];
			else if (Signs[i] == (int)Elements.WATER)
				SlotsGO [i].GetComponent<UnityEngine.UI.Image>().sprite = SlotsColor [(int)Elements.WATER];
			else if (Signs[i] == (int)Elements.EARTH)
				SlotsGO [i].GetComponent<UnityEngine.UI.Image>().sprite = SlotsColor [(int)Elements.EARTH];
			else if (Signs[i] == (int)Elements.AIR)
				SlotsGO [i].GetComponent<UnityEngine.UI.Image>().sprite = SlotsColor [(int)Elements.AIR];
			else if (Signs[i] == (int)Elements.AETHER)
				SlotsGO [i].GetComponent<UnityEngine.UI.Image>().sprite = SlotsColor [(int)Elements.AETHER];
		}

	}

	/*
	protected bool FirePose(Hand gHand)
	{
		bool c1 = FingersExDetect (gHand, true, true, false, false, false);
		Vector3 thumbV = BonesLeapVector (gHand.Fingers [0].bones [3].Center, gHand.Fingers [0].bones [0].Center).ToVector3();
		//Vector3 indexV = BonesLeapVector (gHand.Fingers [1].bones [3].Center , gHand.Fingers [1].bones [0].Center).ToVector3();
		bool c2 = DirectionDetect (thumbV, Camera.main.transform.forward, Camera.main.transform.right * (-1.0f), 30.0f, 30.0f);
		if (c1&&c2)
		{
			return true;
		}else{
			return false;
		}
	}

	*/
	protected bool RedPose(Hand gHand)
	{
		bool c1 = false;
		bool c2 = false;
		bool c3 = false;
		if (VRSettings.isDeviceActive == true) {
			//VR mode
			c1 = FingersExDetect (gHand, false, true, false, false, false);
			Vector3 indexV = BonesLeapVector (gHand.Fingers [1].bones [3].NextJoint, gHand.Fingers [1].bones [0].PrevJoint).ToVector3 ();
			c2 = DirectionDetect (indexV, Camera.main.transform.right, Camera.main.transform.up, 60.0f, 60.0f);
			c3 = DirectionDetect (gHand.PalmNormal.ToVector3 (), Camera.main.transform.forward, 60.0f);

		} else if (VRSettings.isDeviceActive == false){
			//desktop mode
			c1 = FingersExDetect (gHand, true, true, false, false, false);
			Vector3 thumbV = BonesLeapVector (gHand.Fingers [0].bones [3].NextJoint, gHand.Fingers [0].bones [0].PrevJoint).ToVector3 ();
			//Vector3 indexV = BonesLeapVector (gHand.Fingers [1].bones [3].Center , gHand.Fingers [1].bones [0].Center).ToVector3();
			c2 = DirectionDetect (thumbV, Camera.main.transform.forward, Camera.main.transform.right * (-1.0f), 60.0f, 60.0f);
					c3 = DirectionDetect (gHand.PalmNormal.ToVector3 (), Camera.main.transform.up * (-1.0f), 60.0f);
		}
		if (c1&&c2&&c3)
		{
			return true;
		}else{
			return false;
		}
	}

	protected bool YellowPose(Hand gHand)
	{
		// 1) Desktop: indexfinger + middle finger, index finger dirrection = forward
		// 2) VR: indexfinger + middle finger, indexfinger direction = UP
		bool c1 = FingersExDetect (gHand, false, true, true, false, false);
		bool c2 = false;
		bool c3 = false;
		Vector3 indexV = BonesLeapVector (gHand.Fingers [1].bones [3].NextJoint, gHand.Fingers [1].bones [0].PrevJoint).ToVector3();
		if (VRSettings.isDeviceActive == false) {
			//desktop mode
			c2 = DirectionDetect (indexV, Camera.main.transform.right, Camera.main.transform.forward, 60.0f, 60.0f);
			c3 = DirectionDetect (gHand.PalmNormal.ToVector3 (), Camera.main.transform.up * (-1.0f), 60.0f);
		} else if (VRSettings.isDeviceActive == true) {
			//VR mode
			c2 = DirectionDetect (indexV, Camera.main.transform.right, Camera.main.transform.up, 60.0f, 60.0f);
			c3 = DirectionDetect (gHand.PalmNormal.ToVector3 (), Camera.main.transform.forward, 60.0f);

		} 
		if (c1 && c2 &&c3) {
			return true;
		} else {
			return false;
		}
	

	}

	protected bool VioletPose(Hand gHand)
	{
		// 1) Desktop: indexfinger + middle finger + ring finger, index finger dirrection = forward
		// 2) VR: indexfinger + middle finger + ring finger, indexfinger direction = UP
		bool c1 = FingersExDetect (gHand, false, true, true, true, false);
		bool c2 = false;
		bool c3 = false;
		Vector3 indexV = BonesLeapVector (gHand.Fingers [1].bones [3].NextJoint, gHand.Fingers [1].bones [0].PrevJoint).ToVector3();
		if (VRSettings.isDeviceActive == false) {
			//desktop mode
			c2 = DirectionDetect (indexV, Camera.main.transform.right, Camera.main.transform.forward, 60.0f, 60.0f);
			c3 = DirectionDetect (gHand.PalmNormal.ToVector3 (), Camera.main.transform.up * (-1.0f), 60.0f);
		} else if (VRSettings.isDeviceActive == true) {
			//VR mode
			c2 = DirectionDetect (indexV, Camera.main.transform.right, Camera.main.transform.up, 60.0f, 60.0f);
			c3 = DirectionDetect (gHand.PalmNormal.ToVector3 (), Camera.main.transform.forward, 60.0f);

		} 
		if (c1 && c2 && c3) {
			return true;
		} else {
			return false;
		}
	}

	protected bool BrownPose(Hand gHand)
	{	
		bool c1 = false;
		bool c2 = false;
		if (VRSettings.isDeviceActive == true) {
			//VR mode
			c1 = FingersExDetect (gHand, false, false, false, false, false);
			Vector3 palmNormalV = gHand.PalmNormal.ToVector3 ();
			c2 = DirectionDetect (palmNormalV, Camera.main.transform.forward, 60.0f);
		} else if (VRSettings.isDeviceActive == false){
			//desktop mode
			c1 = FingersExDetect (gHand, true, false, false, false, false);
			Vector3 thumbV =  BonesLeapVector (gHand.Fingers [0].bones [3].NextJoint, gHand.Fingers [0].bones [0].PrevJoint).ToVector3 ();
			c2 = DirectionDetect (thumbV, Camera.main.transform.forward, Camera.main.transform.right * (-1.0f), 45.0f, 45.0f);
		}
		if (c1&&c2)
		{
			return true;
		}else{
			return false;
		}
	}

	protected bool BluePose(Hand gHand)
	{
		bool c1 = false;
		bool c2 = false;
		bool c3 = false;
		if (VRSettings.isDeviceActive == true) {
			//VR mode
			c1 = FingersExDetect (gHand, false, true, true, true, true);
			Vector3 indexV = BonesLeapVector (gHand.Fingers [1].bones [3].NextJoint, gHand.Fingers [1].bones [0].PrevJoint).ToVector3();
			c2 = DirectionDetect (indexV, Camera.main.transform.right, Camera.main.transform.up, 60.0f, 60.0f);
			c3 = DirectionDetect (gHand.PalmNormal.ToVector3 (), Camera.main.transform.forward, 60.0f);

		} else if (VRSettings.isDeviceActive == false){
			//desktop mode
			c1 = FingersExDetect (gHand, true, false, false, false, true);
			Vector3 thumbV = BonesLeapVector (gHand.Fingers [0].bones [3].NextJoint, gHand.Fingers [0].bones [0].PrevJoint).ToVector3();
			c2 = DirectionDetect (thumbV, Camera.main.transform.forward, Camera.main.transform.right * (-1.0f), 60.0f, 60.0f);
			c3 = DirectionDetect (gHand.PalmNormal.ToVector3 (), Camera.main.transform.up * (-1.0f), 60.0f);
		}

		if (c1&&c2)
		{
			return true;
		}else{
			return false;
		}
	}

	protected bool OneHandMissleCastSide(Hand gHand)
	{
		float vOn = 1.5f;
		bool c1 = false; 
		bool c2 = false;
		if (gHand.IsRight) {
			c1 = VelocityDet (gHand, vOn, Camera.main.transform.right * (-1.0f));
			c2 = DirectionDetect (gHand.PalmNormal.ToVector3 (), Camera.main.transform.right * (-1.0f), 45.0f);
		} else if (gHand.IsLeft) {
			c1 = VelocityDet (gHand, vOn, Camera.main.transform.right );
			c2 = DirectionDetect (gHand.PalmNormal.ToVector3 (), Camera.main.transform.right, 45.0f);
		}
		if (c1&&c2)
		{
			return true;
		}else{
			return false;
		}
	}

	protected bool OneHandMissleCastForward(Hand gHand)
	{
		float vOn = 1.5f;
		bool c1 = VelocityDet (gHand, vOn, Camera.main.transform.forward);
		bool c2 = DirectionDetect (gHand.PalmNormal.ToVector3 (), Camera.main.transform.forward, 45.0f);
		bool c3 = FingersExDetect (gHand, true, true, true, true, true);

		if (c1&&c2&& c3)
		{
			return true;
		}else{
			return false;
		}
	}
		
	protected bool PowerChargingOneHand(Hand gHand)
	{
		bool c1 = false;
		bool c2 = false;
		bool c3 = false;
		bool c4 = false;
		//bool testCon = FingAnglesDet (hand, 30.0f, 0.0f, 0.0f, 0.0f);
		//bool testCon1 =FingProximalAngleDet (hand, 45.0f, 0.0f, 0.0f, 0.0f, 0.0f);
		//bool testCon3 = FingDistInterToProximalAngleDet (hand, 89.0f, 90.0f, 90.0f, 90.0f, 90.0f);
		if (gHand.IsLeft) {
			c1 = DirectionDetect (gHand.PalmNormal.ToVector3(), Camera.main.transform.right, 45.0f);
		} else if (gHand.IsRight) {
			c1 = DirectionDetect (gHand.PalmNormal.ToVector3(), Camera.main.transform.right*(-1.0f), 45.0f);
		}
		c2 = FingAnglesDet (gHand, 30.0f, 0.0f, 0.0f, 0.0f);
		c3 = FingProximalAngleDet (gHand, 90.0f, 0.0f, 0.0f, 0.0f, 0.0f);
		c4 = FingDistInterToProximalAngleDet (gHand,  120.0f, 90.0f, 90.0f, 90.0f, 90.0f);
		if (c1 && c2 && c3 & c4) {
			return true;
		} else {
			return false;
		}

	}

	protected bool ShieldPoseOneHand (Hand gHand)
	{
		bool c1 = PalmPosDet (gHand, ScreenPos.onTOP);
		//bool c2 = FingersExDetect (gHand,true,true,true,true,true);
		bool c2 = FingersExDetect (gHand,false,false,false,false,false);
		bool c3 = DirectionDetect (gHand.PalmNormal.ToVector3(), Camera.main.transform.forward, 60.0f);
		//bool c3 = FingAnglesDet (gHand, 30.0f, 0.0f, 0.0f, 0.0f);
		if (c1 && c2 && c3) {
			return true;
		} else {
			return false;
		}
	}

	protected bool UpgradeGesture(Hand gHand)
	{
		bool c1 = false;
		bool c2 = false;
		float vOn = 1.0f;
		c1 = DirectionDetect (gHand.PalmNormal.ToVector3 (), Camera.main.transform.up, 45.0f);
		c2 = VelocityDet (gHand, vOn, Camera.main.transform.up);
		if (c1 && c2) {
			//Debug.Log ("UPGRADEGESTURE_ACTIVATED");
			return true;
		} else {
			return false;
		}
	}



}
