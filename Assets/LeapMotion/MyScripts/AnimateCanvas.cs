﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimateCanvas : MonoBehaviour {


	private Vector3 startPos = new Vector3(0.0f,0.0f,0.0f);
	private Vector3 endPos = new Vector3(0.0f,500.0f,0.0f);
	private Vector3 startPosText  = new Vector3(0.0f,-36.0f,0.0f);
	private Vector3 endPosText = new Vector3(0.0f, 464.0f ,0.0f);
	public float speed = 100.0f;
	private float startTime;
	private float journeyLength;
	private float journeyLengthText;

	// Use this for initialization
	void Start () {
		startTime = Time.time;
		journeyLength = Vector3.Distance (startPos, endPos);
		journeyLengthText = Vector3.Distance (startPosText, endPosText);
	}
	
	// Update is called once per frame
	void Update () {
		float distCovered = (Time.time - startTime) * speed;
		float fracJourney = distCovered / journeyLength;
		float fracJourneyText = distCovered / journeyLengthText;
		gameObject.GetComponentInChildren<UnityEngine.UI.Image> ().gameObject.GetComponent<RectTransform> ().anchoredPosition3D = 
			Vector3.Lerp (startPos, endPos, fracJourney);
		gameObject.GetComponentInChildren<Text> ().gameObject.GetComponent<RectTransform> ().anchoredPosition3D = 
			Vector3.Lerp (startPosText, endPosText, fracJourneyText);
		if (gameObject.GetComponentInChildren<UnityEngine.UI.Image> ().gameObject.GetComponent<RectTransform> ().anchoredPosition3D == endPos) {
			Destroy (gameObject);
		}
	}

	void SetCanvas()
	{
//		gameObject.GetComponentInChildren<UnityEngine.UI.Image> ().gameObject.GetComponent<RectTransform> ().position.y;
//		gameObject.GetComponentInChildren<Text> ().gameObject.GetComponent<RectTransform> ();

	}

}
