﻿using UnityEngine;
using UnityEngine.UI;
using ElementsGame;

public enum PadAction {None, Red, Blue, Yellow, Brown, Violet, SmallCast, BigCast, Upgrade, Energy, Shield }

public class PadCanvas : MonoBehaviour {

	public static bool brownAction = false;
	public static bool redAction = false;
	public static bool blueAction = false;
	public static bool yellowAction = false;
	public static bool violletAction = false;
	public static bool upgradeAction = false;
	public static bool smallCastAction = false;
	public static bool bigCastAction = false;
	public static bool energyAction = false;
	public static bool shieldAction = false;

	GameObject canvasAPBackground;
	GameObject canvasAPValue;
	GameObject canvasAPText;

	PadAction currentPadAction = PadAction.None;

	float panelHeight = 30.0f;
	float panelWidth = 333.0f;
	float panelTimer = 10.0f; // set with ProgressBarSet
	float signChargingTime = 2.4f;
	float missileTime = 0.5f;
	float bigMissileTime = 0.8f;
	float upgradeTime = 0.5f;

	// Use this for initialization
	void Start () {
		canvasAPBackground = GameObject.Find ("Canvas_Action_Progress/Panel/Background");
		canvasAPValue = GameObject.Find ("Canvas_Action_Progress/Panel/Value");
		canvasAPText = GameObject.Find ("Canvas_Action_Progress/Panel/Text");

		panelWidth = canvasAPBackground.GetComponent<RectTransform> ().sizeDelta.x;
		ProgressBarReset ();

	}
	
	// Update is called once per frame
	void Update () {
		
		ResetActions ();
		ProgressBarUpdate ();
		ContinueActions (); 
	
	}

	void ProgressBarReset()
	{
		canvasAPValue.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0.0f, panelHeight);
		canvasAPBackground.GetComponent<Image> ().enabled = false;
		canvasAPValue.GetComponent<Image> ().enabled = false;
		canvasAPText.GetComponent<Text> ().enabled = false;
		canvasAPText.GetComponent<Text> ().color = Color.white;
		currentPadAction = PadAction.None;

	}

	void ProgressBarSet(Color barColor, string barText, float barTimer, PadAction pA)
	{
		canvasAPBackground.GetComponent<Image> ().enabled = true;
		canvasAPText.GetComponent<Text> ().enabled = true;
		canvasAPValue.GetComponent<Image> ().enabled = true;
		canvasAPValue.GetComponent<Image> ().color = barColor;
		canvasAPText.GetComponent<Text> ().text = barText;
		panelTimer = barTimer;
		currentPadAction = pA;
	}

	void ProgressBarUpdate()
	{
		if (PadButtons.A_button) {
			ProgressBarSet (GameMaster.brownColor, "Brown Charge", signChargingTime, PadAction.Brown);
		} else if (PadButtons.X_button) {
			ProgressBarSet (Color.blue, "Blue Charge", signChargingTime, PadAction.Blue);
		} else if (PadButtons.Y_button) {
			ProgressBarSet (Color.yellow, "Yellow Charge", signChargingTime, PadAction.Yellow);	
		} else if (PadButtons.B_button) {
			ProgressBarSet (Color.red, "Red Charge", signChargingTime, PadAction.Red);
		} else if (PadButtons.down) {
			ProgressBarSet (GameMaster.violetColor, "Violet Charge", signChargingTime, PadAction.Violet);
		}else if (PadButtons.up){
			ProgressBarSet (Color.black, "Big Missile Cast", bigMissileTime, PadAction.BigCast);
		}else if (PadButtons.left){
			ProgressBarSet (Color.black, "Missile Cast", missileTime, PadAction.SmallCast);
		}else if (PadButtons.right) {
			ProgressBarSet (Color.black, "Upgrade", upgradeTime, PadAction.Upgrade);
		}else {
			ProgressBarReset ();
		}

		//canvasAPValue.GetComponent<RectTransform> ().sizeDelta.x + ();
		canvasAPValue.GetComponent<RectTransform> ().sizeDelta = new Vector2 (canvasAPValue.GetComponent<RectTransform> ().sizeDelta.x + Time.deltaTime / panelTimer * panelWidth , panelHeight );


		if (canvasAPValue.GetComponent<RectTransform> ().sizeDelta.x >= panelWidth) {
			Debug.Log ("PANEL NALADOWANY");
			ProgressBarReady ();
			ProgressBarReset ();

		}
	}

	void ProgressBarReady()
	{
		if (currentPadAction == PadAction.Red) {
			redAction = true;
		} else if (currentPadAction == PadAction.Blue) {
			blueAction = true;
		} else if (currentPadAction == PadAction.Brown) {
			brownAction = true;
		} else if (currentPadAction == PadAction.Yellow) {
			yellowAction = true;
		} else if (currentPadAction == PadAction.Violet) {
			violletAction = true;
		} else if (currentPadAction == PadAction.SmallCast) {
			smallCastAction = true;
		} else if (currentPadAction == PadAction.BigCast) {
			bigCastAction = true;
		} else if (currentPadAction == PadAction.Upgrade) {
			upgradeAction = true;
		}
	}

	void ContinueActions()
	{
		if (PadButtons.RB_button) {
			shieldAction = true;
		} else {
			shieldAction = false;
		}
		if (PadButtons.LB_button) {
			energyAction = true;
		} else {
			energyAction = false;
		}
	}

	void ResetActions()
	{
		brownAction = false;
		 redAction = false;
		 blueAction = false;
         yellowAction = false;
		 violletAction = false;
		 upgradeAction = false;
		 smallCastAction = false;
		 bigCastAction = false;
		 energyAction = false;
		 shieldAction = false;
	}

}
