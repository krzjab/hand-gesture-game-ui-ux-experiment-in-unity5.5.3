﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PadButtons : MonoBehaviour {


	public static bool up; //Xbox360 DPad button
	public static bool down; //Xbox360 DPad button
	public static bool left; //Xbox360 DPad button
	public static bool right; //Xbox360 DPad button
	public static bool A_button = false; // Xbox360 A button
	public static bool B_button = false; // Xbox360 E button
	public static bool X_button = false; // Xbox360 X button
	public static bool Y_button = false; // Xbox360 Y button
	public static bool back_button = false; // Xbox360 back button
	public static bool start_button = false; // Xbox360 start button
	public static bool LB_button = false;// Xbox360 LB button
	public static bool RB_button = false;// Xbox360 RB button
	public static bool LT_button = false; // Xbox360 LT button
	public static bool RT_button = false; // Xbox360 RT button

	private float lastX, lastY;

	void OneButtonActive(ref bool button)
	{
		up = down = left = right = A_button = B_button = X_button = Y_button = back_button = start_button = LB_button = RB_button = LT_button = RT_button = false;
		button = true;
	}

	void Start ()
	{
		up = down = left = right = false;
		//lastX =    lastY = 0;
	}

	void Update()
	{
		//float lastDpadX = lastX;
		//float lastDpadY = lastY;

		float DPadX = Input.GetAxisRaw ("DPad_Horizontal");
		if (DPadX == 1)   { right = true; } else { right = false; }
		if (DPadX == -1) { left = true;  } else { left = false;  }

		//lastX = DPadX;

		float DPadY = Input.GetAxisRaw ("DPad_Vertical");
		if (DPadY == 1)   { up = true;   } else { up = false;   }
		if (DPadY == -1) { down = true; } else { down = false; }

		//lastY = DPadY;
		if (DPadY != 0.0 && DPadX != 0.0) { up = down = left = right = false;}
		if (DPadY != 0.0 || DPadX != 0.0) {Debug.Log ("Up = " + up + " Down  = " + down + " Left = " + left + " Right = " + right);}

		float PadLTRT = Input.GetAxisRaw("Pad_LTRT");
		if (PadLTRT == -1) {	RT_button = true;} else { RT_button = false;}
		if (PadLTRT == 1) {	LT_button = true;} else { LT_button = false;}
		if (RT_button == true || LT_button == true) {Debug.Log ("RT_button = " + RT_button + " LT_button = " + LT_button); }

		//A button
		if (Input.GetKey (KeyCode.JoystickButton0)) {	/*Debug.Log ("Xbox360 A button is Down");*/ OneButtonActive(ref A_button);} else {	A_button = false;}
		//E button
		if (Input.GetKey (KeyCode.JoystickButton1)) {	OneButtonActive(ref B_button);} else {B_button = false;}
		// X button
		if (Input.GetKey (KeyCode.JoystickButton2)) {	OneButtonActive(ref X_button);} else {X_button = false;}
		//Y Button
		if (Input.GetKey (KeyCode.JoystickButton3)) {	OneButtonActive(ref Y_button);} else {Y_button = false;}
		//LB Button
		if (Input.GetKey (KeyCode.JoystickButton4)) {	OneButtonActive(ref LB_button);} else {LB_button = false;}
		//RB Button
		if (Input.GetKey (KeyCode.JoystickButton5)) {	OneButtonActive(ref RB_button);} else {RB_button = false;}
		//Back Button
		if (Input.GetKey (KeyCode.JoystickButton6)) {	OneButtonActive(ref back_button);} else {back_button = false;}
		//Start Button
		if (Input.GetKey (KeyCode.JoystickButton7)) {	OneButtonActive(ref start_button);} else {start_button = false;}


		//Debug.Log ("E_button = " + E_button + "X_button = " + X_button + " Pad_LT = " 	+LT_button+ " Pad_RT = "+ RT_button);


	}
	//how to use:
	//if(PadButtons.up) {
		//some code to execute when the up button is pushed
	//}



}
