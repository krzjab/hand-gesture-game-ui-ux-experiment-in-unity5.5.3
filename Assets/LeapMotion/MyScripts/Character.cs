﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Character Scrip requires Capsule Collider
[RequireComponent(typeof(CapsuleCollider))]

public class Character : MonoBehaviour {



	protected float hpMax = 100.0f;
	protected float hp;
	public float HP { get { return hp; } }
	public float HPMax { get {return hpMax;} }


	protected float dmgMultipRed; // 1=non resistance, 0 = full resistance
	protected float dmgMultipBlue;
	protected float dmgMultipYellow;
	protected float dmgMultipBrown;

	// Use this for initialization
	protected virtual void Start () {
		hp = hpMax;

		SetCollider ();

	}

	virtual protected void SetCollider()
	{
		gameObject.GetComponent<CapsuleCollider> ().radius = 0.5f;
		gameObject.GetComponent<CapsuleCollider> ().height = 1.0f;
	}

	// Update is called once per frame
	void Update () {
		
	}

	virtual public void AddDamage( float dmg, Color dmgColor)
	{
		hp = hp - dmg;
		Debug.Log ("20.0f dmg");
		ElementsGame.GameMaster.CanvasInstantiate ( dmg + " damage" , dmgColor);
		// if HP < 0
	}
}
