﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(LineRenderer))]
public class LineRendererTest : MonoBehaviour {

	GameObject st1;
	GameObject st2;

	public Material redMaterial;

	// Use this for initialization
	void Start () {
		st1 = GameObject.Find ("ST1");
		st2 = GameObject.Find ("ST2");
		gameObject.GetComponent<LineRenderer> ().SetWidth (0.07f, 0.07f);
		gameObject.GetComponent<LineRenderer> ().material.color = Color.white;
		Material redMat = Resources.Load("Materials/Red", typeof(Material)) as Material;
		Material whiteMat = Resources.Load("Materials/White", typeof(Material)) as Material;
		gameObject.GetComponent<LineRenderer>().material = redMat;
		gameObject.GetComponent<MeshRenderer> ().material = whiteMat;

	}
	
	// Update is called once per frame
	void Update () {
		gameObject.GetComponent<LineRenderer> ().SetPosition (0, st1.transform.position);
		gameObject.GetComponent<LineRenderer> ().SetPosition (1, st2.transform.position);
		//gameObject.GetComponent<LineRenderer> ().enabled = true;
	}
		
}
