﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ElementsGame;
//Character Scrip requires Capsule Collider
[RequireComponent(typeof(CapsuleCollider))]

public class EnemyBasic : Character {

	protected SkinnedMeshRenderer[] skinColors;
	/*
	public bool red = false;
	public bool blue = false;
	public bool yellow = false;
	public bool brown = false;
	*/

	// Use this for initialization
	protected override void Start () {
		base.Start();
		//ChangeCharacterColor (Color.yellow);
		//SetDmgMultiplicators (0.0f, 2.0f, 1.0f, 1.0f);


	}

	override protected void SetCollider()
	{
		gameObject.GetComponent<CapsuleCollider> ().radius = 0.5f;
		gameObject.GetComponent<CapsuleCollider> ().height = 3.0f;
	}

	// Update is called once per frame
	void Update () {
		EnemyDeadCheck ();
	}

	override public void AddDamage( float dmg, Color dmgColor)
	{

		float realDmg = 0.0f; //init
		if (dmgColor == Color.red) {
			realDmg = dmg * dmgMultipRed;
		} else if (dmgColor == Color.blue) {
			realDmg = dmg * dmgMultipBlue;
		} else if (dmgColor == Color.yellow) {
			realDmg = dmg * dmgMultipYellow;
		}else if (dmgColor == ElementsGame.GameMaster.brownColor){
			realDmg = dmg * dmgMultipBrown;
		}
		hp = hp - realDmg;
	//	Debug.Log ( realDmg + " dmg");
		if (realDmg < dmg) {
			ElementsGame.GameMaster.CanvasInstantiateEnemy (realDmg + " dmg, " + (dmg - realDmg) + " res", dmgColor);
		}
		if (realDmg > dmg) {
			ElementsGame.GameMaster.CanvasInstantiateEnemy (realDmg + " dmg, " + (realDmg/dmg) + " X dmg", dmgColor);
		}
		//Debug.Log ( realDmg + " dmg");
		if (realDmg == dmg)
			ElementsGame.GameMaster.CanvasInstantiateEnemy ( realDmg + " damage" , dmgColor);	
		


		// if HP < 0

	}

	public void ChangeCharacterColor( Color c)
	{
		skinColors = gameObject.GetComponentsInChildren<SkinnedMeshRenderer> ();

		foreach (SkinnedMeshRenderer sc in skinColors) {
			sc.material.color = c;
		}
	}
		
	public void SetDmgMultiplicators(float red, float blue, float yellow, float brown)
	{
		dmgMultipRed = red;
		dmgMultipBlue = blue;
		dmgMultipYellow = yellow;
		dmgMultipBrown = brown;
	}

	void EnemyDeadCheck()
	{
		if (hp <= 0.0f) {
			EnemyReset ();
			GameMaster.WinLoseCanvasSet (WinLose.Win);
			GameMaster.CanvasInstantiate ("You Win, score: " + GameMaster.scoreWIN, Color.white);
			GameMaster.DestroyMissiles (); // destroy all missiles in the game
			gameObject.GetComponent<NPCEnemyController>().ResetEnemyTimers();
		}
	}
	void EnemyReset()
	{
		hp = hpMax;
	}
}
