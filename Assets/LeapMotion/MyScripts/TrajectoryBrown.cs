﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Leap;
using Leap.Unity;
using UnityEngine.Events;
using UnityEngine.UI;
using ElementsGame;

public class TrajectoryBrown : TrajectoryBase {

	// Use this for initialization
	override protected void Start () {
		base.Start ();
		signMsg = "BROWN SIGN COMPLETED";
		signTextColor = new Color(0.272f,0.171f,0.084f);
		signColor = GameMaster.Elements.EARTH;

	}

	// Update is called once per frame
	void Update () {

	}

	void Subscribe()
	{
		//GameMaster.OnRed += TrajectoryCompleted;
	}

	void UnSubscribe()
	{
		//GameMaster.OnRed -= TrajectoryCompleted;
	}

	void TrajectoryCompleted()
	{

	}

	override protected void TrajectoryDone()
	{
		base.TrajectoryDone();
		//GameMaster.OnRed += RedMessage;
		Debug.Log ("BrownMessage added to OnBrown");
		BrownMessage ();
		// add message for gamemaster
		//RedMessage();
	}

	void BrownMessage()
	{

		Debug.Log("Brown SIGN COMPLETED");
		//tutorial unlockings
		if (GameMaster.Tut_actions_check (7)) {
			GameMaster.Tut_action_set (8,true);
		}

	}


	override protected void SetCheckpoints()
	{
		cp = new GameObject[4];
		cp [0] = GameObject.Find ("SBr1");
		cp [1] = GameObject.Find ("SBr2");
		cp [2] = GameObject.Find ("SBr3");
		cp [3] = GameObject.Find ("SBr4");

	}

	override protected void SetMaterials()
	{
		whiteMat =  Resources.Load("Materials/White", typeof(Material)) as Material;
		signMat = Resources.Load("Materials/Brown", typeof(Material)) as Material;
	}
}
