﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Leap;
using Leap.Unity;
using UnityEngine.Events;
using UnityEngine.UI;
using ElementsGame;

public class TrajectoryBlue : TrajectoryBase {

	// Use this for initialization
	override protected void Start () {
		base.Start ();
		signMsg = "BLUE SIGN COMPLETED";
		signTextColor = Color.blue;
		signColor = GameMaster.Elements.WATER;
	}

	// Update is called once per frame
	void Update () {

	}

	void Subscribe()
	{
		//GameMaster.OnRed += TrajectoryCompleted;
	}

	void UnSubscribe()
	{
		//GameMaster.OnRed -= TrajectoryCompleted;
	}

	void TrajectoryCompleted()
	{

	}

	override protected void TrajectoryDone()
	{
		base.TrajectoryDone();
		//GameMaster.OnRed += RedMessage;
		Debug.Log ("BlueMessage added to OnBlue");
		BlueMessage ();
		// add message for gamemaster
		//RedMessage();
	}

	void BlueMessage()
	{

		Debug.Log("Blue SIGN COMPLETED");
		//tutorial unlockings
		if (GameMaster.Tut_actions_check (4)) {
			GameMaster.Tut_action_set (5,true);
		}

	}


	override protected void SetCheckpoints()
	{
		cp = new GameObject[4];
		cp [0] = GameObject.Find ("SB1");
		cp [1] = GameObject.Find ("SB2");
		cp [2] = GameObject.Find ("SB3");
		cp [3] = GameObject.Find ("SB4");

	}

	override protected void SetMaterials()
	{
		whiteMat =  Resources.Load("Materials/White", typeof(Material)) as Material;
		signMat = Resources.Load("Materials/Blue", typeof(Material)) as Material;
	}
}
