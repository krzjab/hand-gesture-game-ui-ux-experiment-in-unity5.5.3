﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeutralState : FSMState {

	public NeutralState ()
	{
		stateID = FSMStateID.Neutral;
		/*
		dmgMultipRed = 1.0f; //100% dmg
		dmgMultipBlue = 1.0f; // 100% dmg
		dmgMultipYellow = 1.0f; // 100% dmg
		dmgMultipBrown = 1.0f; // 100% dmg
		*/

	}

	public override void DoBeforeEntering()
	{
		GameObject enemy = GameObject.Find("Enemy");
		enemy.GetComponent<EnemyBasic> ().SetDmgMultiplicators (1.0f, 1.0f, 1.0f, 1.0f);
		enemy.GetComponent<EnemyBasic> ().ChangeCharacterColor (Color.black);
	}

	//decide to change state in LateUpdate
	public override void Reason(GameObject player, GameObject npc)
	{
		npc.GetComponent<NPCEnemyController> ().EnemyStateTimeOut ();
		//npc.GetComponent<EnemyBasic> ().ChangeCharacterColor (Color.black);
	}


	// what to do in LateUpdate
	public override void Act(GameObject player, GameObject npc)
	{
		// add randomnes to casting missiles
		int r = Random.Range (0, 100);
		if (r < 25)
			npc.GetComponent<NPCEnemyController> ().EnemyCastMissile (MissileType.RED);
		if (r >= 25 && r < 50)
			npc.GetComponent<NPCEnemyController> ().EnemyCastMissile (MissileType.BLUE);
		if (r >= 50 && r < 75)
			npc.GetComponent<NPCEnemyController> ().EnemyCastMissile (MissileType.YELLOW);
		if (r >= 75 && r < 100)
			npc.GetComponent<NPCEnemyController> ().EnemyCastMissile (MissileType.BROWN);

	}
}
