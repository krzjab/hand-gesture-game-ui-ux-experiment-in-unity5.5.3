﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ElementsGame;

public class RedState : FSMState {

	public RedState ()
	{
		stateID = FSMStateID.Red;
		//start changes
		//odpornosci na ataki ustaw
		/*
		dmgMultipRed = 0.0f; //full resistance
		dmgMultipBlue = 2.0f; // dmg x2
		dmgMultipYellow = 1.0f; // 100% dmg
		dmgMultipBrown = 1.0f; // 100% dmg
		*/

	}
	public override void DoBeforeEntering()
	{
		GameObject enemy = GameObject.Find ("Enemy");
		enemy.GetComponent<EnemyBasic> ().SetDmgMultiplicators (0.0f, 2.0f, 1.0f, 1.0f);
		enemy.GetComponent<EnemyBasic> ().ChangeCharacterColor (Color.red);
	
	}

	//decide to change state in LateUpdate
	public override void Reason(GameObject player, GameObject npc)
	{
		npc.GetComponent<NPCEnemyController> ().EnemyStateTimeOut ();
		//npc.GetComponent<EnemyBasic> ().ChangeCharacterColor (Color.red);


		/*
		//Go back to patrol is it become too far
		else if (dist >= 300.0f)
		{
			Debug.Log("Switch to Patrol state");
			npc.GetComponent<NPCTankController>().SetTransition(Transition.LostPlayer);
		}
		*/
	}


	// what to do in LateUpdate
	public override void Act(GameObject player, GameObject npc)
	{
		// add randomnes to casting missiles
		int r = Random.Range (0, 100);
		if (r < 60)
			npc.GetComponent<NPCEnemyController> ().EnemyCastMissile (MissileType.RED);
		if (r >= 60 && r < 70)
			npc.GetComponent<NPCEnemyController> ().EnemyCastMissile (MissileType.BLUE);
		if (r >= 70 && r < 85)
			npc.GetComponent<NPCEnemyController> ().EnemyCastMissile (MissileType.YELLOW);
		if (r >= 85 && r < 100)
			npc.GetComponent<NPCEnemyController> ().EnemyCastMissile (MissileType.BROWN);
	}
}
