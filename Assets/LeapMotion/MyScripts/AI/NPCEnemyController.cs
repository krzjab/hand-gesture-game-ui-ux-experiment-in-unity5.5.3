﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ElementsGame;

public class NPCEnemyController : AdvancedFSM {


	GameObject objPlayer;
	GameObject objEnemy;

	//Initialize the Finite state machine for the NPC tank
	protected override void Initialize()
	{
		
		//change for testin variations
		elapsedShootTime = 0.0f + 3.0f;
		shootRate = 10.0f;

		//change for testin variations
		elapsedChangeTime = 0.0f;
		changeRate = 20.0f;

		//Get the target enemy(Player)
		objPlayer = GameObject.Find("Player");
		objEnemy = GameObject.Find("Enemy");

		//Start Doing the Finite State Machine
		ConstructFSM();

		Debug.Log (CurrentState + "" + CurrentStateID + " current state and cstate ID");
	}

	//Update each frame
	protected override void FSMUpdate()
	{
		//Check
		elapsedShootTime += Time.deltaTime;
		elapsedChangeTime += Time.deltaTime;
	}

	protected override void FSMFixedUpdate()
	{
		CurrentState.Reason(objPlayer, objEnemy);
		CurrentState.Act(objPlayer , objEnemy);
	}

	public void SetTransition(Transition t) 
	{ 
		PerformTransition(t); 
	}

	private void ConstructFSM()
	{
		
		RedState redState = new RedState ();
		redState.AddTransition (Transition.TimeOut, FSMStateID.Blue);

		BlueState blueState = new BlueState ();
		blueState.AddTransition (Transition.TimeOut, FSMStateID.Yellow);

		YellowState yellowState = new YellowState ();
		yellowState.AddTransition (Transition.TimeOut, FSMStateID.Brown);

		BrownState brownState = new BrownState ();
		brownState.AddTransition (Transition.TimeOut, FSMStateID.Neutral);

		NeutralState neutralState = new NeutralState ();
		neutralState.AddTransition (Transition.TimeOut, FSMStateID.Red);

	
		//Fist added state is the initial state
		AddFSMState (redState);
		AddFSMState (blueState);
		AddFSMState (yellowState);
		AddFSMState (brownState);
		AddFSMState (neutralState);

	}
	/// <summary>
	/// Enemy casts a missile to player character
	/// </summary>
	public void EnemyCastMissile(MissileType mt)
	{
		if (GameMaster.gameMode_PlayGroundMode == false) {
			if (elapsedShootTime >= shootRate) {
				GameMaster.MissileSpawn (Target.Player, GameMaster.speedEnemyMissile, MissileSize.H1, mt);
				elapsedShootTime = 0.0f;
			}
		}
	}
	/// <summary>
	/// Enemies the state time out. 
	/// </summary>
 	public void EnemyStateTimeOut()
	{
		if (elapsedChangeTime >= changeRate) {
			SetTransition (Transition.TimeOut);
			elapsedChangeTime = 0.0f;
		}
	}

	public void ResetEnemyTimers()
	{
		elapsedChangeTime = 0.0f;
		elapsedShootTime = 0.0f+ 2.0f;
	}
}
