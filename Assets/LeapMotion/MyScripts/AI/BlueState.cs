﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueState : FSMState {

	public BlueState ()
	{
		stateID = FSMStateID.Blue;
		//start changes
		//odpornosci na ataki ustaw
		/*
		dmgMultipRed = 2.0f; // 200% dmg
		dmgMultipBlue = 0.0f; // 0% dmg
		dmgMultipYellow = 1.0f; // 100% dmg
		dmgMultipBrown = 1.0f; // 100% dmg
		*/
	
	}

	public override void DoBeforeEntering()
	{
		GameObject enemy = GameObject.Find("Enemy");
		enemy.GetComponent<EnemyBasic> ().SetDmgMultiplicators (2.0f, 0.0f, 1.0f, 1.0f);
		enemy.GetComponent<EnemyBasic> ().ChangeCharacterColor (Color.blue);
	}

	//decide to change state in LateUpdate
	public override void Reason(GameObject player, GameObject npc)
	{
		npc.GetComponent<NPCEnemyController> ().EnemyStateTimeOut ();
		//npc.GetComponent<EnemyBasic> ().ChangeCharacterColor (Color.blue);
	}


	// what to do in LateUpdate
	public override void Act(GameObject player, GameObject npc)
	{
		// add randomnes to casting missiles
		int r = Random.Range (0, 100);
		if (r < 10)
			npc.GetComponent<NPCEnemyController> ().EnemyCastMissile (MissileType.RED);
		if (r >= 10 && r < 70)
			npc.GetComponent<NPCEnemyController> ().EnemyCastMissile (MissileType.BLUE);
		if (r >= 70 && r < 85)
			npc.GetComponent<NPCEnemyController> ().EnemyCastMissile (MissileType.YELLOW);
		if (r >= 85 && r < 100)
			npc.GetComponent<NPCEnemyController> ().EnemyCastMissile (MissileType.BROWN);
	}
}
