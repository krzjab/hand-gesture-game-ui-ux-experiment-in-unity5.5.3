﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrownState : FSMState {

	public BrownState ()
	{
		stateID = FSMStateID.Brown;
		//start changes
		//odpornosci na ataki ustaw
		/*
		dmgMultipRed = 1.0f; //dmg 100%
		dmgMultipBlue = 1.0f; //dmg 100%
		dmgMultipYellow = 2.0f; //dmg 200%
		dmgMultipBrown = 0.0f; // dmg 0%
		*/
	
	}

	public override void DoBeforeEntering()
	{
		GameObject enemy = GameObject.Find("Enemy");
		enemy.GetComponent<EnemyBasic> ().SetDmgMultiplicators (1.0f, 1.0f, 2.0f, 0.0f);
		enemy.GetComponent<EnemyBasic> ().ChangeCharacterColor (ElementsGame.GameMaster.brownColor);
	}

	//decide to change state in LateUpdate
	public override void Reason(GameObject player, GameObject npc)
	{
		npc.GetComponent<NPCEnemyController> ().EnemyStateTimeOut ();
		//npc.GetComponent<EnemyBasic> ().ChangeCharacterColor (ElementsGame.GameMaster.brownColor);
	}


	// what to do in LateUpdate
	public override void Act(GameObject player, GameObject npc)
	{
		// add randomnes to casting missiles
		int r = Random.Range (0, 100);
		if (r < 10)
			npc.GetComponent<NPCEnemyController> ().EnemyCastMissile (MissileType.RED);
		if (r >= 10 && r < 20)
			npc.GetComponent<NPCEnemyController> ().EnemyCastMissile (MissileType.BLUE);
		if (r >= 20 && r < 30)
			npc.GetComponent<NPCEnemyController> ().EnemyCastMissile (MissileType.YELLOW);
		if (r >= 30 && r < 100)
			npc.GetComponent<NPCEnemyController> ().EnemyCastMissile (MissileType.BROWN);
	}
}
