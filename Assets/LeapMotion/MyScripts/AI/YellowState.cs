﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YellowState : FSMState {

	public YellowState ()
	{
		stateID = FSMStateID.Yellow;
		//start changes
		//odpornosci na ataki ustaw
		/*
		dmgMultipRed = 1.0f; //100% dmg
		dmgMultipBlue = 1.0f; // 100% dmg
		dmgMultipYellow = 0.0f; // 0% dmg
		dmgMultipBrown = 2.0f; // 200% dmg
		*/


	}

	public override void DoBeforeEntering()
	{
		GameObject enemy = GameObject.Find("Enemy");
		enemy.GetComponent<EnemyBasic> ().SetDmgMultiplicators (1.0f, 1.0f, 0.0f, 2.0f);
		enemy.GetComponent<EnemyBasic> ().ChangeCharacterColor (Color.yellow);
	}

	//decide to change state in LateUpdate
	public override void Reason(GameObject player, GameObject npc)
	{
		npc.GetComponent<NPCEnemyController> ().EnemyStateTimeOut ();
		//npc.GetComponent<EnemyBasic> ().ChangeCharacterColor (Color.yellow);




	
		//	npc.GetComponent<NPCTankController>().SetTransition(Transition.LostPlayer);
	
	}


	// what to do in LateUpdate
	public override void Act(GameObject player, GameObject npc)
	{
		// add randomnes to casting missiles
		int r = Random.Range (0, 100);
		if (r < 15)
			npc.GetComponent<NPCEnemyController> ().EnemyCastMissile (MissileType.RED);
		if (r >= 15 && r < 30)
			npc.GetComponent<NPCEnemyController> ().EnemyCastMissile (MissileType.BLUE);
		if (r >= 30 && r < 100)
			npc.GetComponent<NPCEnemyController> ().EnemyCastMissile (MissileType.YELLOW);
		
	}
}
