﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ElementsGame;

// Script for a spawned missile,
// MechanicsManager uses a MissileSpawn method

public class Missile : MonoBehaviour {

	public Color missileColor;
	public float missileSpeed;
	public string missileTargetName;

	public MissileType missileType;
	public MissileSize missileSize;

	private float startTime;
	private float journeyLength;
	public Vector3 startPos;
	public Vector3 endPos;



	// Use this for initialization
	void Start () {
		startTime = Time.time;
		journeyLength = Vector3.Distance (startPos, endPos);
		gameObject.GetComponent<MeshRenderer> ().material.color = missileColor;
	}
	
	// Update is called once per frame
	void Update () {
		float distCovered = (Time.time - startTime) * missileSpeed;
		float fracJourney = distCovered / journeyLength;
		gameObject.transform.position = Vector3.Lerp (startPos, endPos, fracJourney);
	}
	void OnTriggerEnter( Collider col)
	{
		if (col.gameObject.name == missileTargetName) {

			//Debug.Log (" Missile Arrived to target");
			Destroy (gameObject);
			if (missileSize == MissileSize.H1) {
				SmallMissileEffect (col);
			} else {
				LargeMissileEffect (col);
			}
			//add demage

		}

	}

	void SmallMissileEffect( Collider c)
	{
		c.gameObject.GetComponent<Character> ().AddDamage (20, missileColor);
	}

	void LargeMissileEffect(Collider c)
	{
		c.gameObject.GetComponent<Character> ().AddDamage (45, missileColor);
	}

	void DestroyMissile()
	{
		Debug.Log ("Missile is destroyed");
		Destroy (gameObject);
	}
}
