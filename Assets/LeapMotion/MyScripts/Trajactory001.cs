﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using Leap;
using Leap.Unity;
using UnityEngine.UI;

// Refactoryzuj, wprowadz liste punktow i funkcje obslugujace ta liste.

public class Trajactory001 : Detector {

	// This is a first test script to make a 3d geometric trajectory from volumens like spheres and cylinders.
	// Script checks if LeapHand position is inside volumens. 
	// Volumens parameters are based on Gameobject points lockated in Unity Scene.

	// p stands for point, c for cylinder, cb for cylinder base, s for sphere
	public GameObject s1;
	public GameObject s1RadiusP;
	public Vector3 s1RV;

	public GameObject c1b1;
	public GameObject c1b2;
	public GameObject c1RadiusP;
	public Vector3 c1RV;

	public GameObject s2;
	public GameObject s2RadiusP;
	public Vector3 s2RV;

	public GameObject c2b1;
	public GameObject c2b2;
	public GameObject c2RadiusP;
	public Vector3 c2RV;

	public GameObject s3;
	public GameObject s3RadiusP;
	public Vector3 s3RV;

	public bool wasActiveEver_s1;
	public bool wasActiveEver_c1;
	public bool wasActiveEver_s2;
	public bool wasActiveEver_c2;
	public bool wasActiveEver_s3;

	public bool isActiveNow_s1;
	public bool isActiveNow_c1;
	public bool isActiveNow_s2;
	public bool isActiveNow_c2;
	public bool isActiveNow_s3;

	LeapProvider provider;

	GameObject canvas_Trajectory001;

	private float Period;//seconds

	public IEnumerator watcherCourtine;

	enum Mode {Mode2D, Mode3D, ModeVR};

	// Use this for initialization
	void Start () {
		Period = 0.10f;
		provider = FindObjectOfType<LeapProvider> () as LeapProvider;
		canvas_Trajectory001 = GameObject.Find ("Canvas_Trajectory001");

		wasActiveEver_s1 = false;
		wasActiveEver_c1 = false;
		wasActiveEver_s2 = false;
		wasActiveEver_c2 = false;
		wasActiveEver_s3 = false;


		StartCoroutine ("SlowUpdateCourtine");

	}

	// Update is called once per frame
	void Update () {
		
	}

	public IEnumerator SlowUpdateCourtine()
	{
		

		while (true) {
			Frame frame = provider.CurrentFrame;
			foreach (Hand hand in frame.Hands) {
				if (!hand.IsLeft) {

					//Sphere1Volume_CalculationAndLogic
					float s1RadiusLength;
					s1RadiusLength = LengthFromVector (s1RadiusP.transform.position - s1.transform.position, Mode.ModeVR);
					float s1ToHandLength;
					s1ToHandLength = LengthFromVector (hand.PalmPosition.ToVector3 () - s1.transform.position, Mode.ModeVR);
					if (s1ToHandLength <= s1RadiusLength){
						isActiveNow_s1 = true;
						wasActiveEver_s1 = true;
					}else{
						isActiveNow_s1 = false;
					}

					// ----sphere1END


					//Cylinder1Volume_CalculationAndLogic
					float lengthFromC1LineToRadiusPoint;
					lengthFromC1LineToRadiusPoint = DFromLine( Mode.ModeVR ,c1b1.transform.position, c1RadiusP.transform.position, c1b1.transform.position, c1b2.transform.position);
					float lengthFromC1LineToHand;
					lengthFromC1LineToHand = DFromLine (Mode.ModeVR ,c1b1.transform.position, hand.PalmPosition.ToVector3(), c1b1.transform.position, c1b2.transform.position);

					float c1DistanceFromBase1;
					c1DistanceFromBase1 = DFromPlane( c1b2.transform.position - c1b1.transform.position, hand.PalmPosition.ToVector3() , c1b1.transform.position );

					float c1DistanceFromBase2;
					c1DistanceFromBase2 = DFromPlane (c1b1.transform.position - c1b2.transform.position, hand.PalmPosition.ToVector3 (), c1b2.transform.position);

					if ((lengthFromC1LineToHand <= lengthFromC1LineToRadiusPoint) && (c1DistanceFromBase1 >= 0.0f) && (c1DistanceFromBase2 >= 0.0f)) {
						isActiveNow_c1 = true;
						wasActiveEver_c1 = true;
					} else {
						isActiveNow_c1 = false;
					}

					//Sphere2Volume_CalculationAndLogic
					float s2RadiusLength;
					s2RadiusLength = LengthFromVector (s2RadiusP.transform.position - s2.transform.position, Mode.ModeVR);
					float s2ToHandLength;
					s2ToHandLength = LengthFromVector (hand.PalmPosition.ToVector3 () - s2.transform.position, Mode.ModeVR);
					if (s2ToHandLength <= s2RadiusLength){
						isActiveNow_s2 = true;
						wasActiveEver_s2 = true;
					}else{
						isActiveNow_s2 = false;
					}

					//Cylinder2Volume_CalculationAndLogic
					float lengthFromC2LineToRadiusPoint;
					lengthFromC2LineToRadiusPoint = DFromLine(Mode.ModeVR, c2b1.transform.position, c2RadiusP.transform.position, c2b1.transform.position, c2b2.transform.position);
					float lengthFromC2LineToHand;
					lengthFromC2LineToHand = DFromLine ( Mode.ModeVR, c2b1.transform.position, hand.PalmPosition.ToVector3(), c2b1.transform.position, c2b2.transform.position);

					float c2DistanceFromBase1;
					c2DistanceFromBase1 = DFromPlane( c2b2.transform.position - c2b1.transform.position, hand.PalmPosition.ToVector3() , c2b1.transform.position );

					float c2DistanceFromBase2;
					c2DistanceFromBase2 = DFromPlane (c2b1.transform.position - c2b2.transform.position, hand.PalmPosition.ToVector3 (), c2b2.transform.position);

					if ((lengthFromC2LineToHand <= lengthFromC2LineToRadiusPoint) && (c2DistanceFromBase1 >= 0.0f) && (c2DistanceFromBase2 >= 0.0f)) 
					{
						isActiveNow_c2 = true;
						wasActiveEver_c2 = true;
					} else {
						isActiveNow_c2 = false;
					}

					//Sphere3Volume_CalculationAndLogic
					float s3RadiusLength;
					s3RadiusLength = LengthFromVector (s3RadiusP.transform.position - s3.transform.position, Mode.ModeVR);
					float s3ToHandLength;
					s3ToHandLength = LengthFromVector (hand.PalmPosition.ToVector3 () - s3.transform.position, Mode.ModeVR);
					if (s3ToHandLength <= s3RadiusLength){
						isActiveNow_s3 = true;
						wasActiveEver_s3 = true;
					}else{
						isActiveNow_s3 = false;
					}


					//Canvas:
					canvas_Trajectory001.GetComponentInChildren<Text> ().text = "S1Radius: " + s1RadiusLength + "\n"+ "S1ToHand: " + s1ToHandLength + "\n" + "S1_Active: " + isActiveNow_s1 + "\n" + "S1_EverActive: " + wasActiveEver_s1+ "\n";
					canvas_Trajectory001.GetComponentInChildren<Text> ().text = canvas_Trajectory001.GetComponentInChildren<Text>().text + "S2Radius: " + s2RadiusLength + "\n"+ "S2ToHand: " + s2ToHandLength + "\n" + "S2_Active: " + isActiveNow_s2 + "\n" + "S2_EverActive: " + wasActiveEver_s2+ "\n";
					canvas_Trajectory001.GetComponentInChildren<Text> ().text = canvas_Trajectory001.GetComponentInChildren<Text>().text + "S3Radius: " + s3RadiusLength + "\n"+ "S3ToHand: " + s3ToHandLength + "\n" + "S3_Active: " + isActiveNow_s3 + "\n" + "S3_EverActive: " + wasActiveEver_s3+ "\n";
					canvas_Trajectory001.GetComponentInChildren<Text> ().text = canvas_Trajectory001.GetComponentInChildren<Text>().text + "C1Radius: " + lengthFromC1LineToRadiusPoint + "\n"+ "C1ToHandDistance: " + lengthFromC1LineToHand + "\n" + "C1Base1ToHand: " + c1DistanceFromBase1 + "\n" + "C1Base2ToHand: " + c1DistanceFromBase2 + "\n" + "C1_Active: " + isActiveNow_c1 + "\n" + "C1_EverActive: " + wasActiveEver_c1+ "\n";
					canvas_Trajectory001.GetComponentInChildren<Text> ().text = canvas_Trajectory001.GetComponentInChildren<Text>().text + "C2Radius: " + lengthFromC2LineToRadiusPoint + "\n"+ "C2ToHandDistance: " + lengthFromC2LineToHand + "\n" + "C2Base1ToHand: " + c2DistanceFromBase1 + "\n" + "C2Base2ToHand: " + c2DistanceFromBase2 + "\n" + "C2_Active: " + isActiveNow_c2 + "\n" + "C2_EverActive: " + wasActiveEver_c2+ "\n";
					canvas_Trajectory001.GetComponentInChildren<Text> ().text = canvas_Trajectory001.GetComponentInChildren<Text> ().text + "\n" + "S1_Active: " + isActiveNow_s1 + "\n" + "C1_Active: " + isActiveNow_c1 + "\n" + "S2_Active: " + isActiveNow_s2 + "\n" + "C2_Active: " + isActiveNow_c2 + "\n" + "S3_Active: " + isActiveNow_s3 + "\n";			
					//Calculations


					//-----------------CEND


					//Logic


					//------------LEND

					//WholeLogicGate
					if (true) {
						Activate ();
					} else {
						Deactivate ();
					}

				}
			}
			yield return new WaitForSeconds (Period);
		}
	}

	float DFromLine (Vector3 linePointS /*On line*/, Vector3 pointP /*Point away form the Line*/, Vector3 lineVecStart, Vector3 lineVecEnd)
	{
		//just 3D mode with no Mode atribute
		Vector3 lineVector = lineVecEnd - lineVecStart;
		return LengthFromVector (Vector3.Cross (pointP - linePointS, lineVector) / LengthFromVector (lineVector));
	}

	float DFromLine ( Mode mode ,Vector3 linePointS /*On line*/, Vector3 pointP /*Point away form the Line*/, Vector3 lineVecStart, Vector3 lineVecEnd)
	{
		Vector3 lineVector = lineVecEnd - lineVecStart;
		switch (mode) 
		{
		case Mode.Mode2D: 
				//instruction
				return LengthFromVector (Vector3.Cross (pointP - linePointS, lineVector), Mode.Mode2D) / LengthFromVector (lineVector, Mode.Mode2D) ;
			break;
		case Mode.Mode3D:
				//instruction
				return LengthFromVector (Vector3.Cross (pointP - linePointS, lineVector), Mode.Mode3D) / LengthFromVector (lineVector, Mode.Mode3D);
			break;
		case Mode.ModeVR:
				lineVector = Vector3.ProjectOnPlane (lineVector, Camera.main.transform.forward);
				Vector3 pointsVec = pointP - linePointS;
				pointsVec = Vector3.ProjectOnPlane (pointsVec, Camera.main.transform.forward);
				return LengthFromVector (Vector3.Cross (pointsVec, lineVector), Mode.Mode3D) / LengthFromVector (lineVector, Mode.Mode3D);
			break;
		default:
			return 0;
			break;
		}



	}

	float DFromPlane( Vector3 normalVector, Vector3 pointP, Vector3 planePointS)
	{	
		//just 3D test is enough
		float alfa = -Vector3.Dot (normalVector, planePointS);
		return  (Vector3.Dot(normalVector, pointP) + alfa) / LengthFromVector(normalVector) ;
	}

	float LengthFromVector(Vector3 vec)
	{
		
		return Mathf.Sqrt(Mathf.Pow(vec.x,2)+Mathf.Pow(vec.y,2)+Mathf.Pow(vec.z,2));
	}

	float LengthFromVector(Vector3 vec, Mode mode)
	{
		
		switch (mode) {
		case Mode.Mode2D:
			return Mathf.Sqrt(Mathf.Pow(vec.x,2)+Mathf.Pow(vec.y,2));;
			break;
		case Mode.Mode3D:
			return Mathf.Sqrt (Mathf.Pow (vec.x, 2) + Mathf.Pow (vec.y, 2) + Mathf.Pow (vec.z, 2));
			break;
		case Mode.ModeVR:
			// get instruction
			Vector3 vOnCamPro;
			vOnCamPro = Vector3.ProjectOnPlane (vec, Camera.main.transform.forward);
			return Mathf.Sqrt (Mathf.Pow (vOnCamPro.x, 2) + Mathf.Pow (vOnCamPro.y, 2) + Mathf.Pow (vOnCamPro.z, 2));
			break;
		default:
			return 0;
			break;
		}

	}


}
