﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Leap;
using Leap.Unity;
using ElementsGame;

public class TrajectoryBase : MonoBehaviour {


	public GameObject[] cp;
	public List<VolumeT> volumens;

	public GameObject testSfereCenter;
	public GameObject testSfereRange;
	public float sphereRadiusLength;

	public GameObject testCylinderRangePoint;

	public bool trajIsActive;
	public bool trajCal_isActive;

	LeapProvider provider;
	private float Period = 0.01f;//seconds

	protected Material whiteMat;
	protected Material signMat;

	protected string signMsg = "TEST Sign MSG";
	protected Color signTextColor = Color.black;
	protected GameMaster.Elements signColor = GameMaster.Elements.FIRE;

	virtual protected void SignCanvasMessage()
	{
		Debug.Log(signMsg);
		GameMaster.CanvasInstantiate (signMsg, signTextColor);
	}

	// Use this for initialization
	 protected virtual void Start () {
		testSfereRange.GetComponent<Renderer> ().enabled = false; // make rangeSfere not visible
		SetCheckpoints ();
		SetMaterials ();
		SetRenderersOnStart ();
		provider = FindObjectOfType<LeapProvider> () as LeapProvider; // get provider
		trajIsActive = false;
		trajCal_isActive = false;
		VolumensSet ();
		//CheckPointsTest ();
		//StartCoroutine (CheckPointsSlowTest ());
		sphereRadiusLength = ElMath.LengthFromVector (testSfereRange.transform.position - 
			testSfereCenter.transform.position, ElMath.Mode.ModeVR);
		StartCoroutine ("SlowUpdateCorutine"); // Main Procedure
	}

	void VolumensSet()
	{
		int volumensAmount = (int)2.0 * (cp.Length) - (int)1.0;
		Debug.Log("VolumensList initialization");
		volumens = new List<VolumeT>();
		Debug.Log ("VolumensCount =" + volumens.Count);
		for (int i =0; i < volumensAmount; i++)
		{
			volumens.Add (new VolumeT ( ));
		}
		Debug.Log ("VolumensCount =" + volumens.Count);
		Debug.Log ("VolumensCount from list= " + volumens.Count);
		for (int i = 0; i < volumensAmount; i++) 
		{
			int rest = i % 2;
			if (rest == 0 || i ==0) {
				volumens[i].volumenObj = cp[i/2];
				//Debug.Log("number of volume aded to list= " +i);
			} else if (rest == 1) {
				volumens[i].volumenObj = cp[(i-1)/2];
				//Debug.Log("number of volume aded to list= " +i);
			}
		}
		Debug.Log ("VolumensCount from list= " + volumens.Count);
	}

	// Update is called once per frame
	void Update () {
		
	}

	public void CheckPointsTest()
	{
		foreach (VolumeT v in volumens) {
			Debug.Log ("Volumens Transform position x= " + v.volumenObj.transform.position.x +
				"Volumens Transform local position x= " + v.volumenObj.transform.localPosition.x +
				"Volumens Transform position y= " +v.volumenObj.transform.position.y +
				"Volumens Transform local position y= " +v.volumenObj.transform.localPosition.y +
				"Volumens Transform position z" +v.volumenObj.transform.position.z +
				"Volumens Transform local position y= " +v.volumenObj.transform.localPosition.z 

			);
		}
	}
	public IEnumerator CheckPointsSlowTest()
	{
		while (true) {
			foreach (VolumeT v in volumens) {
				Debug.Log ("Name= "+ v.volumenObj.name + "Volumens Transform position x= " + v.volumenObj.transform.position.x +
					"Name= "+ v.volumenObj.name +"Volumens Transform local position x= " + v.volumenObj.transform.localPosition.x +
					"Name= "+ v.volumenObj.name +"Volumens Transform position y= " + v.volumenObj.transform.position.y +
					"Name= "+ v.volumenObj.name +"Volumens Transform local position y= " + v.volumenObj.transform.localPosition.y +
					"Name= "+ v.volumenObj.name +"Volumens Transform position z" + v.volumenObj.transform.position.z +
					"Name= "+ v.volumenObj.name +"Volumens Transform local position y= " + v.volumenObj.transform.localPosition.z 
				);
			}
			yield return new WaitForSeconds (5.0f);
		}
	}

	//for 1 frame!
	void SphereCalculation(Hand ht, int volNumber)
	{
		if (trajCal_isActive) {
			float sToHandLength;
			sToHandLength = ElMath.LengthFromVector (ht.PalmPosition.ToVector3 () - 
				volumens [volNumber].volumenObj.transform.position, ElMath.Mode.ModeVR);
			if (sToHandLength <= sphereRadiusLength) {
				if (volumens.Count > volNumber + 1) {
					volumens [volNumber + 1].isVisible = true;
				}
				if (volNumber != 0) {
					if (volumens [volNumber - 1].wasActive == true) {
						volumens [volNumber].isActive = true;
						volumens [volNumber].wasActive = true;
					} 
				} else if (volNumber == 0) {
					volumens [volNumber].isActive = true;
				}
			} else {
				volumens [volNumber].isActive = false;
			}
		}
	}
	//for 1 frame!volumen 0
	void SphereCalculation (Hand ht)
	{
		if (trajIsActive && trajCal_isActive == false) {

			//Sphere1Volume_CalculationAndLogic
			float sToHandLength;
			sToHandLength = ElMath.LengthFromVector (ht.PalmPosition.ToVector3 () - volumens[0].volumenObj.transform.position, ElMath.Mode.ModeVR);
			if (sToHandLength <= sphereRadiusLength){
				volumens[0].isActive = true;
				volumens [0].isVisible = true;
				volumens[0].wasActive = true;
				trajCal_isActive = true;
			}else{
				volumens[0].isActive = false;
			}
			// ----sphere1END
		}
	}

	// for 1 frame!
	void CylinderCalculation(Hand ht, int volNumber)
	{
		//Cylinder1Volume_CalculationAndLogic
		Transform cBase1 = volumens[volNumber].volumenObj.transform;
		Transform cBase2 = volumens[volNumber+1].volumenObj.transform;
		float lengthFromC1LineToRadiusPoint;
		lengthFromC1LineToRadiusPoint = sphereRadiusLength*0.9f;
		float lengthFromC1LineToHand;
		lengthFromC1LineToHand = ElMath.DFromLine (ElMath.Mode.ModeVR , cBase1.position,
			ht.PalmPosition.ToVector3(),  cBase1.position, cBase2.position);
		float c1DistanceFromBase1;
		c1DistanceFromBase1 = ElMath.DFromPlane(cBase2.position - cBase1.position,
			ht.PalmPosition.ToVector3() , cBase1.position );
		float c1DistanceFromBase2;
		c1DistanceFromBase2 =ElMath.DFromPlane (cBase1.position - cBase2.position,
			ht.PalmPosition.ToVector3 (), cBase2.position);
		if ((lengthFromC1LineToHand <= lengthFromC1LineToRadiusPoint) && 
			(c1DistanceFromBase1 >= 0.0f) && (c1DistanceFromBase2 >= 0.0f)) {
			if (volumens.Count > volNumber + 1 && volumens [volNumber - 1].wasActive == true) {
				volumens [volNumber + 1].isVisible = true;
			}
			if (volumens [volNumber - 1].wasActive == true) {
				volumens [volNumber].isActive = true;
				volumens [volNumber].wasActive = true;
			} 
		} else {
			volumens [volNumber].isActive = false;
		}
	}
		
	protected void TrajectoryCalculations( Hand hand)
	{
		//calculation for volumen[0], spherecalculation and visualization
		if (trajIsActive == true && trajCal_isActive == false) {
			SphereCalculation (hand);
			volumens[0].isVisible = true;
			SphereVisualization (0);
		}
		//calculation for volumen[i], spherecalculation, cylindercalculation and visualization
		if (trajCal_isActive && trajIsActive) {
			int wasActiveCount = 0;
			int isActiveCount = 0;
			for (int i = 0; i < volumens.Count; i++) 
			{
				int rest = i % 2;
				if (rest == 0 || i ==0) {
					// Logic for Spheres_volumens
					SphereCalculation (hand, i);
					SphereVisualization (i);
				} else if (rest == 1) {
					// Logic for Cylinders_volumens
					CylinderCalculation (hand, i);
					CylinderVisualization (i);
				}
				if (volumens [i].wasActive == true) {
					wasActiveCount++;
				}
				if (volumens [i].isActive == true) {
					isActiveCount++;
				}
			}
			if (isActiveCount == 0) {
				TrajectoryDeactivateDebug ();
				TrajectoryReset ();
			}
			if (wasActiveCount == volumens.Count)
			{
				TrajectoryDone(); //Check if all checkpoints were Active and Send Message
			}
		}
	}

	void TrajectoryReset()
	{
		ResetRenderers ();
		ResetVolumens ();
		trajIsActive = false;
		trajCal_isActive = false;
	}
	virtual protected void TrajectoryDone()
	{
		SignCanvasMessage ();
		TrajectoryReset ();
		GameObject.Find("GameMaster").GetComponent<GameMaster>().SignsAdd (signColor);
		//MSG for GameMaster
	}


	void ResetVolumens()
	{
		for (int i = 0; i < volumens.Count; i++) {
			volumens [i].isActive = false;
			volumens [i].isVisible = false;
			volumens [i].wasActive = false;
		}
	}

	protected void SphereVisualization(int volNumber)
	{
		if (volumens [volNumber].isVisible == true && volumens[volNumber].wasActive == false) {
			volumens [volNumber].volumenObj.GetComponent<MeshRenderer>().enabled = true;
			volumens [volNumber].volumenObj.GetComponent<MeshRenderer> ().material = whiteMat;

		} else if(volumens [volNumber].isVisible == true && volumens[volNumber].wasActive == true)
		{
			volumens [volNumber].volumenObj.GetComponent<MeshRenderer>().enabled = true;
			volumens [volNumber].volumenObj.GetComponent<MeshRenderer> ().material = signMat;
		}
	}

	protected void CylinderVisualization(int volNumber)
	{
		if (volumens [volNumber].isVisible == true && volumens[volNumber].wasActive == false) {
			//set white material
			volumens [volNumber].volumenObj.GetComponent<LineRenderer> ().material = whiteMat;
			//set first point to draw line
			volumens [volNumber].volumenObj.GetComponent<LineRenderer> ().SetPosition (0, volumens [volNumber].volumenObj.transform.position);
			//set second point to draw line
			volumens [volNumber].volumenObj.GetComponent<LineRenderer> ().SetPosition (1, volumens [volNumber+1].volumenObj.transform.position);
			// enable drawing line
			volumens [volNumber].volumenObj.GetComponent<LineRenderer> ().enabled = true;

		} else if(volumens [volNumber].isVisible == true && volumens[volNumber].wasActive == true)
		{
			//set sign material
			volumens [volNumber].volumenObj.GetComponent<LineRenderer> ().material = signMat;
			//set first point to draw line
			volumens [volNumber].volumenObj.GetComponent<LineRenderer> ().SetPosition (0, volumens [volNumber].volumenObj.transform.position);
			//set second point to draw line
			volumens [volNumber].volumenObj.GetComponent<LineRenderer> ().SetPosition (1, volumens [volNumber+1].volumenObj.transform.position);
			// enable drawing line
			volumens [volNumber].volumenObj.GetComponent<LineRenderer> ().enabled = true;
		}
	}

	virtual protected void SetCheckpoints()
	{
		cp = new GameObject[3];
		cp [0] = GameObject.Find ("ST1");
		cp [1] = GameObject.Find ("ST2");
		cp [2] = GameObject.Find ("ST3");

	}

	virtual protected void SetMaterials()
	{
		whiteMat =  Resources.Load("Materials/White", typeof(Material)) as Material;
		signMat = Resources.Load("Materials/Green", typeof(Material)) as Material;
	}

	protected void SetRenderersOnStart()
	{
			foreach (GameObject _checkp in cp) {	
				// error if no GameObject in array, 
				// error when Checkpoints are not Active in Inspector View
				_checkp.GetComponent<MeshRenderer> ().enabled = false;
				_checkp.GetComponent<MeshRenderer> ().material = whiteMat;
				LineRenderer _cpLineRend = _checkp.GetComponent<LineRenderer> ();
				_cpLineRend.SetWidth (0.07f, 0.07f);
				_cpLineRend.material.color = Color.white;
				_cpLineRend.material = whiteMat;
				_cpLineRend.enabled = false;
			}
	}
	protected void ResetRenderers()
	{
		foreach (GameObject _checkp in cp)
		{
			_checkp.GetComponent<MeshRenderer> ().enabled = false;
			_checkp.GetComponent<MeshRenderer> ().material = whiteMat;
			LineRenderer _cpLineRend = _checkp.GetComponent<LineRenderer> ();
			_cpLineRend.material = whiteMat;
			_cpLineRend.enabled = false;
		}
	}

	public IEnumerator SlowUpdateCorutine()
	{
		while (true) {
			Frame frame = provider.CurrentFrame;
			foreach (Hand hand in frame.Hands) {
				if (!hand.IsLeft) {
					TrajectoryCalculations (hand);
				}
			}
			yield return new WaitForSeconds (Period);
		}
	}

	public void ActivateTra()
	{
		trajIsActive = true;


	}

	public void DeactivateTra()
	{
		TrajectoryDeactivateDebug ();
		TrajectoryReset ();
	}

	private void TrajectoryDeactivateDebug()
	{
		if (volumens[0].wasActive == true) {
			Debug.Log (gameObject.name + " Trajectory Deactivated ");
			GameMaster.CanvasInstantiate ("Sign Failure", Color.black);
		}
	}

}
