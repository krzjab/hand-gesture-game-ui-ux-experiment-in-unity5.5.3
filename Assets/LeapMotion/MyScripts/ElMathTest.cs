﻿using UnityEngine;
using System.Collections;

public class ElMathTest : MonoBehaviour {

	Vector3 sV01;
	Vector3 sV02;
	Vector3 sV03;

	// Use this for initialization
	void Start () {
		sV01 = new Vector3 (1.0f, 0.0f, 0.0f);
		Debug.Log ("startVector001 Values = " + "x= " + sV01.x + " y= " + sV01.y + " z= " + sV01.z);
		sV01 = ElMath.RotOnAxis (sV01, new Vector3(0.0f, 3.0f, 0.0f), 90.0f);
		Debug.Log ("startVector001 Values = " + "x= " + sV01.x + " y= " + sV01.y + " z= " + sV01.z);
		Debug.Log ("sV01 Length " + Vector3.Magnitude (sV01));

		Debug.Log ("PlaneProjection");
		sV01 = new Vector3 (0.8f, 3.49f, 12.0f);
		Debug.Log ("startVector001 Values = " + "x= " + sV01.x + " y= " + sV01.y + " z= " + sV01.z);
		sV01 = ElMath.ProjectOnPlane (sV01, new Vector3 (0.45f, -1.0f, 0.0f));
		Debug.Log ("My Projection Method");
		Debug.Log ("startVector001 Values = " + "x= " + sV01.x + " y= " + sV01.y + " z= " + sV01.z);

		Debug.Log ("Reset Vector");
		sV01 = new Vector3 (0.8f, 3.49f, 12.0f);
		Debug.Log ("startVector001 Values = " + "x= " + sV01.x + " y= " + sV01.y + " z= " + sV01.z);
		Debug.Log("UnityProjection Method");
		sV01 = Vector3.ProjectOnPlane(sV01, new Vector3 (0.45f, -1.0f, 0.0f));
		Debug.Log ("startVector001 after unity projection Values = " + "x= " + sV01.x + " y= " + sV01.y + " z= " + sV01.z);

		Debug.Log ("Unity Projection on line = " + Vector3.Project (sV01, new Vector3 (4.0f, -2.0f, -1.0f)) + " My projection method = " + ElMath.Project (sV01, new Vector3 (4.0f, -2.0f, -1.0f)));
	}
	
	// Update is called once per frame
	void Update () {
	
	}




}
