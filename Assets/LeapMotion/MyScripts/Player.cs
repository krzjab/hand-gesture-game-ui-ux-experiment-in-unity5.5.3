﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ElementsGame;

public enum Shield {None,Red,Blue, Yellow, Brown};

//Character Scrip requires Capsule Collider
[RequireComponent(typeof(CapsuleCollider))]

public class Player : Character {

	protected float epMax = 100.0f;
	protected float ep;
	public float EP { get { return ep; } }
	public float EPMax { get {return epMax;} }


	protected float spMax = 100.0f;
	protected float sp;
	public float SP { get { return sp; } }
	public float SPMax { get {return spMax;} }

	protected Shield currentShield;
	public bool shieldKinetic = false;

	public Image shieldImage;
	public GameObject shieldImageObject;

	protected float epRestoreRate = 7.0f;
	protected float spRestoreRate = 5.0f;

	private bool tutoriaCheck_ep =false;
	private bool tutoriaCheck_sp =false;

	// Use this for initialization
	protected override void Start () {
		base.Start ();
		//ep = epMax;
		//sp = spMax;
		if (GameMaster.gameMode_TutorialGroundMode == false) {
			ep = epMax;
			sp = spMax;
		}
		if (GameMaster.gameMode_TutorialGroundMode == true)
		{	//testing  - set to zero
			ep = 0.0f;
			sp = 0.0f;
		}

		shieldImageObject = GameObject.Find ("Canvas_Shield/Panel/Shield");
		// test shield on start
		SetShield (Shield.None);
	}
	
	// Update is called once per frame
	void Update () {
		DamageMultiplicationUpdate ();
		if (GameMaster.gameMode_TutorialGroundMode == false) {
			EnergyRestore ();
			ShieldRestore ();
		} 
		if (GameMaster.gameMode_TutorialGroundMode == true && GameMaster.Tut_actions_check (8)) {
			EnergyRestore ();
			if (ep >= 100.0f && tutoriaCheck_ep ==false) {
				GameMaster.Tut_action_set (9, true);
				tutoriaCheck_ep = true;
			}
		}
		if (GameMaster.gameMode_TutorialGroundMode == true && GameMaster.Tut_actions_check (9)) {
			ShieldRestore ();
			if (sp >= 50.0f && tutoriaCheck_sp == false) {
				GameMaster.Tut_action_set (10, true);
				tutoriaCheck_sp = true;
			}
		}
		if (tutoriaCheck_ep == true) {
			EnergyRestore ();
		}
		if (tutoriaCheck_sp == true) {
			ShieldRestore ();
		}

		PlayerDeadCheck ();

	}

	public void EnergyCost(float epCost)
	{
		ep = ep - epCost;
	}

	public void EnergyRestore()
	{
		if (ep <= epMax) {
			if (GameMaster.powerChargingLeft && GameMaster.powerChargingRight) {
				ep += Time.deltaTime * epRestoreRate * 2.5f;
			} else if (GameMaster.powerChargingLeft || GameMaster.powerChargingRight) {
				ep += Time.deltaTime * epRestoreRate * 1.0f;
			}
		}
	}
	public void ShieldRestore()
	{
		if (sp <= spMax && ep >=0.0f) {
			if (GameMaster.shieldPoseHandLeft && GameMaster.shieldPoseHandRight) {
				sp += Time.deltaTime * spRestoreRate * 1.4f;
				ep -= Time.deltaTime * epRestoreRate * 3.0f;
			} else if (GameMaster.shieldPoseHandRight || GameMaster.shieldPoseHandLeft) {
				sp += Time.deltaTime * spRestoreRate * 0.6f;
				ep -= Time.deltaTime * epRestoreRate * 1.5f;
			}
		}
	}

	/// <summary>
	/// Sets the shield. To resist missiles.
	/// </summary>
	/// <param name="sh">Sh.</param>
	public void SetShield(Shield sh)
	{
		float shieldEnergyCost = GameMaster.upgradeEPCost;

			currentShield = sh;
			Debug.Log (currentShield.ToString ());
			shieldImageObject.GetComponent<Image> ().enabled = true;
			//add image to shield canvas
			if (currentShield == Shield.None) {
				//shieldImage.enabled = false;
				shieldImageObject.GetComponent<Image> ().sprite = null;
				shieldImageObject.GetComponent<Image> ().enabled = false;
			}
		if (ep >= shieldEnergyCost && sh != Shield.None ) {
			if (currentShield == Shield.Red)
				shieldImageObject.GetComponent<Image> ().sprite = Resources.Load ("Sprites/redShield", typeof(Sprite)) as Sprite;
			if (currentShield == Shield.Blue)
				shieldImageObject.GetComponent<Image> ().sprite = Resources.Load ("Sprites/blueShield", typeof(Sprite)) as Sprite;
			if (currentShield == Shield.Yellow)
				shieldImageObject.GetComponent<Image> ().sprite = Resources.Load ("Sprites/yellowShield", typeof(Sprite)) as Sprite;
			if (currentShield == Shield.Brown)
				shieldImageObject.GetComponent<Image> ().sprite = Resources.Load ("Sprites/brownShield", typeof(Sprite)) as Sprite;
			EnergyCost (shieldEnergyCost);
		} else if(ep< shieldEnergyCost && sh != Shield.None)  {
			GameMaster.CanvasInstantiate ("Need " + shieldEnergyCost + " energy", new Color (148.0f, 0.0f, 211.0f));
		}
	}


	public void ShieldAdd(float amountOfShieldToRestore)
	{
		sp += amountOfShieldToRestore;
	}

	override public void AddDamage( float dmg, Color dmgColor)
	{	
		

		float realDmg = 0.0f; //init
		float shieldDMG = 0.0f;
		float shieldDMGMultiplicator = 2.0f;
		float colorDmgMultip = 0.0f;

		if (dmgColor == Color.red) {
			colorDmgMultip = dmgMultipRed;
		} else if (dmgColor == Color.blue) {
			colorDmgMultip = dmgMultipBlue;
		} else if (dmgColor == Color.yellow) {
			colorDmgMultip = dmgMultipYellow;
		} else if (dmgColor == ElementsGame.GameMaster.brownColor) {
			colorDmgMultip = dmgMultipBrown;
		} else if (dmgColor == Color.white) {
			colorDmgMultip = 1.0f;
		}
		realDmg = dmg * colorDmgMultip;

		if (sp > 0.0f) {
			if (realDmg * shieldDMGMultiplicator <= sp) {
				// only shield DMG
				shieldDMG = realDmg * shieldDMGMultiplicator;
				sp -= shieldDMG;
				if (colorDmgMultip < 1.0f) {
					ElementsGame.GameMaster.CanvasInstantiatePlayer (shieldDMG + " sDmg, " + (dmg - shieldDMG) + " res", dmgColor); // color dmg multiplicator < 1
				}
				if (colorDmgMultip > 1.0f) {
					ElementsGame.GameMaster.CanvasInstantiatePlayer (shieldDMG + " sDmg, " + shieldDMG/dmg + " X dmg", dmgColor); // color dmg multiplicator > 1
				}
				//Debug.Log ( realDmg + " dmg");
				if (colorDmgMultip == 1.0f)
					ElementsGame.GameMaster.CanvasInstantiatePlayer (shieldDMG + " sDamage", dmgColor);	// dmg multiplicator =1
			} else if (realDmg * shieldDMGMultiplicator > sp) {
				// shield dmg and hp dmg
				shieldDMG = sp;
				realDmg = realDmg - (sp/shieldDMGMultiplicator);
				hp = hp - realDmg;
				sp = 0.0f;
				if (colorDmgMultip == 0.0f) {
					ElementsGame.GameMaster.CanvasInstantiatePlayer ((int)realDmg + " Dmg, "+(int)shieldDMG + " sDmg" + (int)(dmg - realDmg) + " res", dmgColor); // color dmg multiplicator < 1
				}
				if (colorDmgMultip > 1.0f) {
					ElementsGame.GameMaster.CanvasInstantiatePlayer ((int)realDmg + " Dmg, "+(int)shieldDMG + " sDmg" + colorDmgMultip + " X dmg", dmgColor); // color dmg multiplicator > 1
				}
				//Debug.Log ( realDmg + " dmg");
				if (colorDmgMultip== 1.0f)
					ElementsGame.GameMaster.CanvasInstantiatePlayer ((int)realDmg + " Dmg "+ (int)shieldDMG + " sDmg", dmgColor);	//  dmg multiplicator  = 1
			}

			
		} else if (sp == 0.0f) {
			//hp dmg only
			hp = hp - realDmg;
			if (realDmg < dmg) {
				ElementsGame.GameMaster.CanvasInstantiatePlayer (realDmg + " dmg, " + (dmg - realDmg) + " res", dmgColor);
			}
			if (realDmg > dmg) {
				ElementsGame.GameMaster.CanvasInstantiatePlayer (realDmg + " dmg, " + (realDmg/dmg) + " X dmg", dmgColor);
			}
			//Debug.Log ( realDmg + " dmg");
			if (realDmg == dmg)
				ElementsGame.GameMaster.CanvasInstantiatePlayer ( realDmg + " damage" , dmgColor);	
		}
			


		//currentShield set to None 
		SetShield(Shield.None);

		// if HP < 0
	}

	void DamageMultiplicationUpdate()
	{
		if (currentShield == Shield.None) {
			dmgMultipRed = 1.0f;
			dmgMultipBlue = 1.0f;
			dmgMultipYellow = 1.0f;
			dmgMultipBrown = 1.0f;
		}
		if (currentShield == Shield.Blue) {
			dmgMultipRed = 0.0f;
			dmgMultipBlue = 2.0f;
			dmgMultipYellow = 1.0f;
			dmgMultipBrown = 1.0f;
		}
		if (currentShield == Shield.Red) {
			dmgMultipRed = 2.0f;
			dmgMultipBlue = 0.0f;
			dmgMultipYellow = 1.0f;
			dmgMultipBrown = 1.0f;
		}
		if (currentShield == Shield.Brown) {
			dmgMultipRed = 1.0f;
			dmgMultipBlue = 1.0f;
			dmgMultipYellow = 0.0f;
			dmgMultipBrown = 2.0f;
		}
		if (currentShield == Shield.Yellow) {
			dmgMultipRed = 1.0f;
			dmgMultipBlue = 1.0f;
			dmgMultipYellow = 2.0f;
			dmgMultipBrown = 0.0f;
		}

	}

	void PlayerDeadCheck()
	{
		if (hp <= 0.0f) {
			ResetPlayer ();
			GameMaster.CanvasInstantiate ("You Lose", Color.black);
			GameMaster.WinLoseCanvasSet (WinLose.Lose);
			GameMaster.DestroyMissiles (); // destroy all missiles in the game
			GameMaster.SignsReset();
			GameObject.Find ("Enemy").GetComponent<NPCEnemyController> ().ResetEnemyTimers ();
		}
	}
	void ResetPlayer()
	{
		hp = hpMax;
		ep = epMax;
		sp = spMax;
	}

}
