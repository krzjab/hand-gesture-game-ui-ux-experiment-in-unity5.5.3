﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ElementsGame;

public class PlayGround : Ground {

	void Awake()
	{
		GameMaster.gameMode_PlayGroundMode = true;	
		GameMaster.gameMode_test_isActive = false;
		GameMaster.gameMode_TutorialGroundMode = false;
	}

	// Use this for initialization
	override protected void Start () {
		base.Start ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
