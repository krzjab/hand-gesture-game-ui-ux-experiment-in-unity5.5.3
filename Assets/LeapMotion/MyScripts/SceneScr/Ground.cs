﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground : MonoBehaviour {

	protected GameObject pointer0;
	protected GameObject pointer1;



	// Use this for initialization
	virtual protected void Start () {
		StartCoroutine (LateStart (0.1f));
	}

	IEnumerator LateStart(float waitTime)
	{
		yield return new WaitForSeconds (waitTime);
		pointer0 = GameObject.Find ("Pointer 0");
		pointer0.transform.position.Set(0.0f, 0.0f, -100.0f);
		pointer0.GetComponent<Renderer>().enabled = false;

		pointer1 = GameObject.Find ("Pointer 1");
		pointer1.transform.position.Set(0.0f, 0.0f, -100.0f);
		pointer1.GetComponent<Renderer>().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
