﻿using UnityEngine;
using ElementsGame;
using UnityEngine.UI;

public class TutorialGround : Ground {



	//MovieTextures
	MovieTexture[] mts = new MovieTexture[23];
	MovieTexture currentMT;

	// Canvas text msg's
	string[] instructions = new string[23];
	string currentIns;

	// Grats for unlocking
	string[] grats = new string[23];

	//Pads info
	string[] padInfos = new string[23];

	//Action Trigger
	bool[] actionTrigger =  new bool[23]; // to make a if statement to block making action more then one time

	//Canvas GameObjects
	GameObject canvasInstructionPanel;
	GameObject canvasMoviePanel; 


	//----------------------------


	void MovieTextures_set()
	{
		mts [0] =  Resources.Load("Movies/01_MC_small", typeof(MovieTexture)) as MovieTexture;
		//mts[1] = Resources.Load("Movies/01_MC_small", typeof(MovieTexture)) as MovieTexture;
		mts[1] = Resources.Load("Movies/02_U_small", typeof(MovieTexture)) as MovieTexture;
		mts[2] = Resources.Load("Movies/03_BMC_small", typeof(MovieTexture)) as MovieTexture;
		mts[3] = Resources.Load("Movies/04_R_small", typeof(MovieTexture)) as MovieTexture;
		mts[4] = Resources.Load("Movies/05_Blu_small", typeof(MovieTexture)) as MovieTexture;
		mts[5] = Resources.Load("Movies/06_Y_small", typeof(MovieTexture)) as MovieTexture;
		mts[6] = Resources.Load("Movies/07_V_small", typeof(MovieTexture)) as MovieTexture;
		mts[7] = Resources.Load("Movies/08_Br_small", typeof(MovieTexture)) as MovieTexture;
		mts[8] = Resources.Load("Movies/09_Ch_small", typeof(MovieTexture)) as MovieTexture;
		mts[9] = Resources.Load("Movies/10_Sh_small", typeof(MovieTexture)) as MovieTexture;
		mts[10] = Resources.Load("Movies/11_RM_small", typeof(MovieTexture)) as MovieTexture;
		mts[11] = Resources.Load("Movies/12_BluM_small", typeof(MovieTexture)) as MovieTexture;
		mts[12] = Resources.Load("Movies/13_YM_small", typeof(MovieTexture)) as MovieTexture;
		mts[13] = Resources.Load("Movies/14_BrM_small", typeof(MovieTexture)) as MovieTexture;
		mts[14] = Resources.Load("Movies/15_RMb_small", typeof(MovieTexture)) as MovieTexture;
		mts[15] = Resources.Load("Movies/16_BluMb_small", typeof(MovieTexture)) as MovieTexture;
		mts[16] = Resources.Load("Movies/17_YMb_small", typeof(MovieTexture)) as MovieTexture;
		mts[17] = Resources.Load("Movies/18_BrMb_small", typeof(MovieTexture)) as MovieTexture;
		mts[18] = Resources.Load("Movies/19_RU_small", typeof(MovieTexture)) as MovieTexture;
		mts[19] = Resources.Load("Movies/20_BluU_small", typeof(MovieTexture)) as MovieTexture;
		mts[20] = Resources.Load("Movies/21_YU_small", typeof(MovieTexture)) as MovieTexture;
		mts[21] = Resources.Load("Movies/22_BrU_small", typeof(MovieTexture)) as MovieTexture;
		mts[22] = Resources.Load("Movies/22_BrU_small", typeof(MovieTexture)) as MovieTexture;

		currentMT = mts [0];
	}



	void InstructionSet()
	{
		grats [0]  = "";
		instructions [0]  = "Welcom to Tutorial Ground. \n"
		+ "Your first task is to make a MISSILE CAST gesture. "
		+ "Turn right hand palm to the left and speed it in that direction. "
		+ " You can try to make this with left hand in the right direction.";
		padInfos [0]  = "\nAre you using a XBOX 360 Pad? Push LEFT and hold.";

		grats [1] = "Congratulations! Well made MISSILE CAST gesture. \n";
		instructions [1] = "Your second task is to make a UPGRADE gesture. "
		+ "Put your hand low and turn the palm up. "
		+ "Speed it in the UP direction.";
		padInfos [1] = "\nAre you using a XBOX 360 Pad? Push RIGHT and hold.";

		grats [2] = "Congratulations! Well made UPGRADE gesture. \n";
		instructions [2] = "Your third task is to make a DOUBLE HAND MISSILE CAST gesture. "
			+ "Put both hands in front of you. Extend all fingers. " 
			+ "Turn palms insides in the front direction. "
			+ "Speed hands in the front direction.";
		padInfos [2] = "\nAre you using a XBOX 360 Pad? Push UP and hold.";

		grats [3] = "Congratulations! Well made DOUBLE HAND MISSILE CAST gesture. \n";
		instructions [3] = " Your fourth task is to make a RED SIGN gesture. "
		+ "Extand your index finger and point it in UP direction."
		+" Palms inside has to point forward. "
		+ "Look for a white dot on the screen "
		+ "move your hand MIDDLE point to it and follow the path "
		+ "that appear.";
		padInfos [3] = "\nAre you using a XBOX 360 Pad? Push B and hold.";

		grats [4] = "Congratulations! Well made RED SIGN gesture. \n"
			+ "Look down you got a red charge.\n";
		instructions [4] = " Your fifth task is to make a BLUE SIGN gesture. "
			+ "Extand your index, middle, ring and pinky fingers and point index finger in UP direction. "
			+" Palms inside has to point forward. "
			+ "Look for a white dot on the screen. "
			+ "move your hand MIDDLE point to it and follow the path "
			+ "that appear.";
		padInfos [4] = "\nAre you using a XBOX 360 Pad? Push X and hold.";

		grats [5] = "Congratulations! Well made BLUE SIGN gesture. \n"
			+ "Look down you got a blue charge.\n";
		instructions [5] = " Your sixth task is to make a YELLOW SIGN gesture. "
			+ "Extand your index and middle fingers and point index finger in UP direction "
			+" Palms inside has to point forward "
			+ "Look for a white dot on the screen. "
			+ "move your hand MIDDLE point to it and follow the path "
			+ "that appear.";
		padInfos [5] = "\nAre you using a XBOX 360 Pad? Push Y and hold.";

		grats [6] = "Congratulations! Well made YELLOW SIGN gesture. \n"
			+ "Look down you got a yellow charge. \n";
		instructions [6] = " Your seventh task is to make a VIOLET SIGN gesture. "
			+ "Extand your index, middle and ring fingers and point index finger in UP direction. "
			+" Palms inside has to point forward. "
			+ "Look for a white dot on the screen, "
			+ "move your hand MIDDLE point to it and follow the path "
			+ "that appear.";
		padInfos [6] = "\nAre you using a XBOX 360 Pad? Push DOWN and hold.";

		grats [7] = "Congratulations! Well made VIOLET SIGN gesture. \n"
			+ "Look down you got a violet charge. \n";
		instructions [7] = " Your eight task is to make a BROWN SIGN gesture. "
			+ "Close your hand and put it in front of you "
			+" Palms inside has to point forward. "
			+ "Look for a white dot on the screen. "
			+ "move your hand MIDDLE point to it and follow the path "
			+ "that appear.";
		padInfos [7] = "\nAre you using a XBOX 360 Pad? Push A and hold.";

		grats [8] = "Congratulations! Well made BROWN SIGN gesture. \n"
			+ "Look down you got a brown charge. \n";
		instructions [8] = " To cast missiles and make upgrades you need energy points. "
		+ "To get more of them, you need to make a CHARGING gesture. "
		+ "Put your right hand in front of you. Turn palms inside to the left."
		+ "Push all of the fingers too the back of your palm."
		+ "Try to make a shape like a claw. "
		+ "Make the same with left hand, just turn it in the right direction."
		+ "Your task is to: Recharge 100 energy ponts.";
		padInfos [8] = "\nAre you using a XBOX 360 Pad? Push LB and hold.";

		grats [9] = "Congratulations! Your energy bar is full.\n";
		instructions [9] = "Now it is time o learn about SHIELD POINTS. "
		+ "Thay can absorb damage from missiles casted to you by your enemies."
		+ "In opposition to health points, shield points can be recharged."
		+ "You need to make a SHIELD gesture. Put your hand in front of you"
		+ ", close it (make a fist) and put it in the upper half of the view."
		+ "\nYour task: Recharge 50 shield points";
		padInfos [9] = "\nAre you using a XBOX 360 Pad? Push RB and hold.";

		grats[10] = "Congratulations! You got enough shield points.\n";
		instructions [10] = " Lets try to  cast a RED MISSILE.  "
		+ "To do this you need 15 energy points and a red charge"
		+ "Try to make a RED SIGN gesture and then a "
		+ "MISSILE CAST gesture."
		+ "RED MISSILE makes extra damage to blue enemies but red enemies "
		+ "are resistant to it.";
		padInfos [10] = "\nAre you using a XBOX 360 Pad? Hold B and than hold LEFT.";

		grats[11] = "Congratulations! You casted a RED MISSILE.\n";
		instructions [11] = " Lets try to  cast a BLUE MISSILE. "
			+ "To do this you need 15 energy points and a blue charge"
			+ "Try to make a BLUE SIGN gesture and then a "
			+ "MISSILE CAST gesture."
			+ "BLUE MISSILE makes extra damage to red enemies but blue enemies "
			+ "are resistant to it.";
		padInfos [11] = "\nAre you using a XBOX 360 Pad? Hold X and than hold LEFT.";

		grats[12] = "Congratulations! You casted a BLUE MISSILE.\n";
		instructions [12] = " Lets try to  cast a YELLOW MISSILE. "
			+ "To do this you need 15 energy points and a yellow charge"
			+ "Try to make a YELLOW SIGN gesture and then a "
			+ "MISSILE CAST gesture."
			+ "YELLOW MISSILE makes extra damage to brown enemies but yellow enemies "
			+ "are resistant to it.";
		padInfos [12] = "\nAre you using a XBOX 360 Pad? Hold Y and than hold LEFT.";

		grats[13] = "Congratulations! You casted a YELLOW MISSILE.\n";
		instructions [13] = " Lets try to  cast a BROWN MISSILE.  "
			+ "To do this you need 15 energy points and a brown charge"
			+ "Try to make a BROWN SIGN gesture and then a "
			+ "MISSILE CAST gesture."
			+ "BROWN MISSILE makes extra damage to yellow enemies but brown enemies "
			+ "are resistant to it.";
		padInfos [13] = "\nAre you using a XBOX 360 Pad? Hold A and than hold LEFT.";

		grats[14] = "Congratulations! You casted a BROWN MISSILE.\n";
		instructions [14] = " Lets try to  cast a BIG RED MISSILE. "
			+ "Remember, every Big missile needs a VIOLET SIGN on the 2nd charge slot. "
			+ "You need 25 energy points, red and violet charge."
			+ "Try to make a RED SIGN gesture, VIOLET SIGN gesture and then a "
			+ "DOUBLE HAND MISSILE CAST gesture."
			+ "RED MISSILE makes extra damage to blue enemies but red enemies "
			+ "are resistant to it.";
		padInfos [14] = "\nAre you using a XBOX 360 Pad? Hold B, then hold DOWN and lastly hold UP.";

		grats[15] = "Congratulations! You casted a BIG RED MISSILE.\n";
		instructions [15] = " Lets try to  cast a BIG BLUE MISSILE. "
			+ "Remember, every Big missile needs a VIOLET SIGN on the 2nd charge slot. "
			+ "You need 25 energy points, blue and violet charge."
			+ "Try to make a BLUE SIGN gesture, VIOLET SIGN gesture and then a "
			+ "DOUBLE HAND MISSILE CAST gesture."
			+ "BLUE MISSILE makes extra damage to red enemies but blue enemies "
			+ "are resistant to it.";
		padInfos [15] = "\nAre you using a XBOX 360 Pad? Hold X, then hold DOWN and lastly hold UP.";

		grats[16] = "Congratulations! You casted a BIG BLUE MISSILE.\n";
		instructions [16] = "Lets try to  cast a BIG YELLOW MISSILE. "
			+ "Remember, every Big missile needs a VIOLET SIGN on the 2nd charge slot. "
			+ "You need 25 energy points, yellow and violet charge."
			+ "Try to make a YELLOW SIGN gesture, VIOLET SIGN gesture and then a "
			+ "DOUBLE HAND MISSILE CAST gesture."
			+ "YELLOW MISSILE makes extra damage to brown enemies but yellow enemies "
			+ "are resistant to it.";
		padInfos [16] = "\nAre you using a XBOX 360 Pad? Hold Y, then hold DOWN and lastly hold UP.";

		grats[17] = "Congratulations! You casted a BIG YELLOW MISSILE.\n";
		instructions [17] = "Lets try to  cast a BIG BROWN MISSILE. "
			+ "Remember, every Big missile needs a VIOLET SIGN on the 2nd charge slot. "
			+ "You need 25 energy points, brown and violet charge."
			+ "Try to make a BROWN SIGN gesture, VIOLET SIGN gesture and then a "
			+ "DOUBLE HAND MISSILE CAST gesture."
			+ "BROWN MISSILE makes extra damage to yellow enemies but brown enemies "
			+ "are resistant to it.";
		padInfos [17] = "\nAre you using a XBOX 360 Pad? Hold A, then hold DOWN and lastly hold UP.";

		grats[18] = "Congratulations! You casted a BIG BROWN MISSILE.\n";
		instructions [18] = "Lets try to  cast a RED SHIELD UPGRADE. "
			+ "To do this you need 10 energy points and red charge."
			+ "Try to make a RED SIGN gesture and then a "
			+ "UPGRADE gesture."
			+ "RED SHIELD UPGRADE disables one blue missile from the enemy, "
			+ "but a red missile will give you 2x damage.";
		padInfos [18] = "\nAre you using a XBOX 360 Pad? Hold B, then hold RIGHT.";

		grats[19] = "Congratulations! You casted a RED SHIELD UPGRADE.\n";
		instructions [19] = "Lets try to cast a BLUE SHIELD UPGRADE. "
			+ "To do this you need 10 energy points and blue charge."
			+ "Try to make a BLUE SIGN gesture and then a "
			+ "UPGRADE gesture."
			+ "BLUE SHIELD UPGRADE disables one red missile from the enemy,"
			+ "but a blue missile will give you 2x damage.";
		padInfos [19] = "\nAre you using a XBOX 360 Pad? Hold X, then hold RIGHT.";

		grats[20] = "Congratulations! You casted a BLUE SHIELD UPGRADE.\n";
		instructions [20] = "Lets try to cast a YELLOW SHIELD UPGRADE. "
			+ "To do this you need 10 energy points and yellow charge."
			+ "Try to make a YELLOW SIGN gesture and then a "
			+ "UPGRADE gesture."
			+ "YELLOW SHIELD UPGRADE disables one brown missile from the enemy."
			+ "but a yellow missile will give you 2x damage.";
		padInfos [20] = "\nAre you using a XBOX 360 Pad? Hold Y, then hold RIGHT.";

		grats[21] = "Congratulations! You casted a YELLOW SHIELD UPGRADE.\n";
		instructions [21] = "Lets try to cast a BROWN SHIELD UPGRADE. "
			+ "To do this you need 10 energy points and brown charge."
			+ "Try to make a BROWN SIGN gesture and then a "
			+ "UPGRADE gesture."
			+ "BROWN SHIELD UPGRADE disables one yellow missile from the enemy."
			+ "but a brown missile will give you 2x damage.";
		padInfos [21] = "\nAre you using a XBOX 360 Pad? Hold A, then hold RIGHT.";


		grats[22] = "Congratulations! You finished the tutorial!\n"
			+ "You can stay and practice here."
			+ "To return to Main Menu push the EXIT button on your left side."
			+ "If you are using XBOX 360 Pad press BACK to exit to MENU."
			+ "From Menu chose PLAY GROUND to practice gestures or"
			+ " FIGHT GROUND to challenge your skills.";
		instructions [22] = "";
		padInfos [22] = "";
	}

	void SetCanvas()
	{
		canvasMoviePanel = GameObject.Find ("Canvas_TGround/Panel2");
		canvasInstructionPanel = GameObject.Find ("Canvas_TGround/Panel1");
	}

	void SetTriggersOnStart()
	{
		for (int i = 0; i < 23; i++) {
			actionTrigger [i] = true;
		}

	}

	void SetCanvasNr(int canvasNr)
	{
		if (GameMaster.gameMode_TutorialGroundMode == true) {
			if (GameMaster.tut_actions [canvasNr] == true && actionTrigger[canvasNr] == true) {
				//Set and Play the Movie Img
				canvasMoviePanel.GetComponentInChildren<RawImage>().enabled = true;
				canvasMoviePanel.GetComponentInChildren<RawImage>().texture = mts [canvasNr];
				currentMT = (MovieTexture)canvasMoviePanel.GetComponentInChildren<RawImage> ().mainTexture;
				currentMT.loop = true;
				currentMT.Play ();
				//Set the Instruction
				currentIns = grats[canvasNr] + instructions [canvasNr] + padInfos [canvasNr];
				canvasInstructionPanel.GetComponentInChildren<Text> ().enabled = true;
				canvasInstructionPanel.GetComponentInChildren<Text> ().text = currentIns;

				// One time trigger
				actionTrigger [canvasNr] = false;// disable this canvas set
			}
		}
	}


	void SetCanvasLoop()
	{
		for (int i = 0; i < 23; i++) {
			SetCanvasNr (i);
		}
	}


	void Awake()
	{

		GameMaster.gameMode_TutorialGroundMode = true;
		GameMaster.gameMode_PlayGroundMode = true;	
		GameMaster.gameMode_test_isActive = false;

		MovieTextures_set ();
		InstructionSet ();
		SetCanvas ();
		SetTriggersOnStart ();
	}

	// Use this for initialization
	override protected void Start () {
		base.Start ();
		MovieTextures_set ();
		InstructionSet ();
		SetCanvas ();
		SetTriggersOnStart ();


	}
	
	// Update is called once per frame
	void Update () {
		SetCanvasLoop ();

	}
}
