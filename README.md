**About**

(Project from 2017)

Project, implementation and verification of a video game prototype which is using a Leap Motion controller. Two virtual reality user interfaces were made (for Leap Motion and pad Xbox360). There was made a set of interaction methods to augment gameplay and its mechanics (7 detection methods, 5 sign drawing, 10 gesture types with combining capability, 14 gameplay actions). Immersion and user interface studies (with players collaboration) were made. Tools which were used: Unity v5.5.3, C#, Leap Motion Orion SDK 3.2, Leap Motion Unity Core Assets 4.1.4, Leap Motion Modules: Detection Examples, UIInput. Leap Motion and Xbox360 pad user interfaces showed to have a good, comparable quality. No significant changes in immersion were noticed.

**Scripts Locations**

Assets/LeapMotion/MyScripts/...

**Images, Diagrams**

https://drive.google.com/drive/folders/1ClyS9FHrnWCjSzxyHNaOSjd1PuYksm6f?usp=sharing

**Movie presentation**

https://drive.google.com/drive/folders/1ImMN4KCAv4beD806kw9hGUXojJmZkOB8?usp=sharing
